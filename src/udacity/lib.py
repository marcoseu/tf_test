
from __future__ import print_function

import os
import numpy as np
import hickle
import time
from texttable import Texttable


class Util(object):
    DATA_DIR = "../data"
    OUTPUT_DIR = "../output"

    FILE_ORIG = os.path.join(OUTPUT_DIR, "notMNIST_orig.hickle.gz")
    FILE_TRAINING = os.path.join(OUTPUT_DIR, "notMNIST_training.hickle.gz")
    FILE_REFORMATTED = os.path.join(OUTPUT_DIR, "notMNIST_reformatted.hickle.gz")

    LABELS = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J"]

    @classmethod
    def get_data_path(cls, dir_name):
        return os.path.join(cls.DATA_DIR, dir_name)

    @classmethod
    def get_directories(cls, data_path):
        res = []
        for entry in os.listdir(data_path):
            found_path = os.path.join(data_path, entry)
            if os.path.isdir(found_path):
                res.append(found_path)
        return res

    @classmethod
    def save_var(cls, var, file_name):
        hickle.dump(var, file_name, mode='w', compression='gzip')

    @classmethod
    def load_var(cls, file_name):
        return hickle.load(file_name)

    @classmethod
    def compute_stat(cls, dataset, labels):
        # If dataset is None, return array of the key
        if dataset is None:
            return ["tensor", "mean", "stdev", "label_tensor"]
        else:
            return {
                "tensor": dataset.shape,
                "mean": np.mean(dataset),
                "stdev": np.std(dataset),
                "label_tensor": labels.shape
            }

    @classmethod
    def print_stat(cls, stat):
        print('  - Full dataset tensor:', stat['tensor'])
        print('    Mean:', stat['mean'])
        print('    Standard deviation:', stat['stdev'])
        print('    Labels:', stat['label_tensor'])

    @classmethod
    def print_stats(cls, data):
        data_keys = data.keys()
        headers = cls.compute_stat(None, None)

        # Create stat rows
        rows = [[""] + headers]
        for key in data_keys:
            stat = cls.compute_stat(data[key]["dataset"], data[key]["labels"])
            row = [key]
            for header in headers:
                row.append(stat[header])
            rows.append(row)

        tbl = Texttable()
        tbl.add_rows(rows)

        print(tbl.draw())


class Timer(object):
    _start = None

    def __init__(self):
        self._start = time.time()

    def stop(self):
        now = time.time()
        if self._start is None:
            raise Exception("Timer not started")
        else:
            start = self._start
            self._start = None
            return now - start
