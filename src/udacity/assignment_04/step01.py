#!/usr/bin/env python

'''
https://github.com/tensorflow/tensorflow/blob/master/tensorflow/examples/udacity/4_convolutions.ipynb
'''


from __future__ import print_function

import os
import sys
import math
import numpy as np
import tensorflow as tf
from six.moves import range

from common import Util as util
from common import Timer

image_size = 28
num_labels = 10
num_channels = 1

LOG_DIR = "./log/"
LEARNING_RATE = 0.05
NUM_STEPS = 1001
BETA_REGULARIZATION = 0.001

CONV_PATH_SIZE = 5
CONV_DEPTH = 16
CONV_STDDEV = 0.1
HIDDEN_SIZE = 64

LAYERS = [
    {
        "type": "conv",
        "weights": tf.Variable(tf.truncated_normal(
            [CONV_PATH_SIZE, CONV_PATH_SIZE, num_channels, CONV_DEPTH],
            stddev=CONV_STDDEV
        )),
        "biases": tf.Variable(tf.zeros([CONV_DEPTH])),
        "stride": [1, 1, 1, 1],
        "padding": "SAME",
    },
    {
        "type": "conv",
        "weights": tf.Variable(tf.truncated_normal(
            [CONV_PATH_SIZE, CONV_PATH_SIZE, CONV_DEPTH, CONV_DEPTH],
            stddev=CONV_STDDEV
        )),
        "biases": tf.Variable(tf.constant(1.0, shape=[CONV_DEPTH])),
        "stride": [1, 1, 1, 1],
        "padding": "SAME"
    },
    {
        "type": "reshape_hidden"
    },
    {
        "type": "hidden",
        "weights": tf.Variable(tf.truncated_normal(
            [image_size // 4 * image_size // 4 * CONV_DEPTH, HIDDEN_SIZE],
            stddev=CONV_STDDEV
        )),
        "biases": tf.Variable(tf.constant(1.0, shape=[HIDDEN_SIZE]))
    },
    {
        "type": "output",
        "weights": tf.Variable(tf.truncated_normal(
            [HIDDEN_SIZE, num_labels],
            stddev=CONV_STDDEV
        )),
        "biases": tf.Variable(tf.constant(1.0, shape=[num_labels]))
    }
]

DROPOUT = 0.5


def accuracy(predictions, labels):
    return (
        100.0 * np.sum(
            np.argmax(predictions, 1) == np.argmax(labels, 1)
        ) / predictions.shape[0]
    )


def reformat(dataset, labels):
    '''
    Reshape by adding one more dimention of num_channels size.
    -1 is a special variable to "the size of that dimension is computed so that the total size remains constan"
    '''
    dataset = dataset.reshape(
        (-1, image_size, image_size, num_channels)
    ).astype(np.float32)
    labels = (np.arange(num_labels) == labels[:, None]).astype(np.float32)
    return dataset, labels


def build_layers(start_dataset, dataset_labels=None):
    dataset = start_dataset

    # Last layer type must be output
    if LAYERS[-1]["type"] != "output":
        raise Exception("A layer called 'output' must be defined")

    # Create hidden layers
    loss = None
    lcounter = 0
    for layer in LAYERS:
        # print("processing layer %s: %d" % (layer["type"], lcounter))
        # print("dataset shape:", dataset.get_shape())
        lcounter += 1
        if layer["type"] == "conv":
            conv = tf.nn.conv2d(dataset, layer["weights"], layer["stride"], padding=layer["padding"])
            hidden = tf.nn.relu(conv + layer["biases"])
            layer["logit"] = tf.nn.max_pool(hidden, [1, 2, 2, 1], [1, 2, 2, 1], padding=layer["padding"])
        elif layer["type"] == "reshape_hidden":
            shape = dataset.get_shape().as_list()
            dim_images = shape[0]
            dim_features = 1
            for dim in shape[1:]:
                dim_features = dim_features * dim
            layer["logit"] = tf.reshape(dataset, [dim_images, dim_features])
        elif layer["type"] == "hidden":
            layer["logit"] = tf.nn.relu(tf.matmul(dataset, layer["weights"]) + layer["biases"])
        elif layer["type"] == layer["type"] == "output":
            layer["logit"] = tf.matmul(dataset, layer["weights"]) + layer["biases"]

        dataset = layer["logit"]

        # layer["logit"] = tf.nn.dropout(logit, DROPOUT)
        # layer["logit"] = logit

    # Create output
    if dataset_labels is not None:
        loss = tf.reduce_mean(
            tf.nn.softmax_cross_entropy_with_logits(dataset, dataset_labels)
        )

    # Compute beta regularization
    # loss_r2 = loss + BETA_REGULARIZATION * tf.nn.l2_loss(weights)
    # loss_r2_beta = 0
    # for layer in LAYERS["output"]:
    #     loss_r2_beta += BETA_REGULARIZATION * tf.nn.l2_loss(layer["weights"])
    # loss_r2 = loss + BETA_REGULARIZATION * tf.nn.l2_loss(weights) + loss_r2_beta

    return dataset, loss


def run_StochasticGradientDescentRELU_conv(reformatted):
    print("### Running StochasticGradientDescentRELU")
    train_dataset = reformatted["train"]["dataset"]
    train_labels = reformatted["train"]["labels"]
    valid_dataset = reformatted["valid"]["dataset"]
    valid_labels = reformatted["valid"]["labels"]
    test_dataset = reformatted["test"]["dataset"]
    test_labels = reformatted["test"]["labels"]

    batch_size = 16

    # with graph.as_default():

    # Define step variable
    tf_init_steps = tf.Variable(0, tf.int32)

    # Input data. For the training data, we use a placeholder that will be fed
    # at run time with a training minibatch.
    tf_train_dataset = tf.placeholder(
        tf.float32, shape=(batch_size, image_size, image_size, num_channels))
    tf_train_labels = tf.placeholder(tf.float32, shape=(batch_size, num_labels))
    tf_valid_dataset = tf.constant(valid_dataset)
    tf_test_dataset = tf.constant(test_dataset)

    # Build layers
    logits, loss = build_layers(
        tf_train_dataset, tf_train_labels
    )
    print("logits: %s" % logits.get_shape())

    # Optimizer
    # global_step = tf.Variable(0)
    # learn_rate = tf.train.exponential_decay(LEARNING_RATE, global_step, 1, 0.9999)
    optimizer = tf.train.GradientDescentOptimizer(LEARNING_RATE).minimize(loss)

    # Predictions for the training, validation, and test data.
    train_prediction = tf.nn.softmax(logits)

    # Validation: return logit from build_layers()
    valid_prediction = tf.nn.softmax(build_layers(tf_valid_dataset)[0])

    # Test
    test_prediction = tf.nn.softmax(build_layers(tf_test_dataset)[0])

    print(" ## Starting training sample using run_StochasticGradientDescentRELU_deep")
    timer = Timer()
    # saver = tf.train.Saver()

    with tf.Session() as session:
        tf.initialize_all_variables().run()
        print("Initialized")

        '''
        ckpt = tf.train.get_checkpoint_state("%s/saver" % LOG_DIR)
        if ckpt:
            saver.restore(session, ckpt.model_checkpoint_path)
            print("Restored Session with step %s" % tf_init_steps.eval())
        else:
            tf.initialize_all_variables().run()
            print("Initialized with step: %s" % tf_init_steps.eval())
        '''

        for step in range(tf_init_steps.eval(), NUM_STEPS):
            # Pick an offset within the training data, which has been randomized.
            # Note: we could use better randomization across epochs.
            offset = (step * batch_size) % (train_labels.shape[0] - batch_size)
            # Generate a minibatch.
            batch_data = train_dataset[offset:(offset + batch_size), :, :, :]
            batch_labels = train_labels[offset:(offset + batch_size), :]
            # Prepare a dictionary telling the session where to feed the minibatch.
            # The key of the dictionary is the placeholder node of the graph to be fed,
            # and the value is the numpy array to feed to it.
            feed_dict = {tf_train_dataset: batch_data, tf_train_labels: batch_labels}
            _, l, predictions = session.run(
                [optimizer, loss, train_prediction], feed_dict=feed_dict)
            if (step % 50 == 0):
                print("Minibatch loss at step %d: %f" % (step, l))
                print("Minibatch accuracy: %.1f%%" % accuracy(predictions, batch_labels))
                print("Validation accuracy: %.1f%%" % accuracy(
                    valid_prediction.eval(), valid_labels))
                tf_init_steps.assign(step)
                session.run(tf_init_steps)
                # saver.save(session, "%s/saver" % LOG_DIR, global_step=step+1)
        print("Test accuracy: %.1f%%" % accuracy(test_prediction.eval(), test_labels))
    print("  - Training completed in %.2f secs" % timer.stop())

#
# RUN THE SCRIPT
#
if __name__ == "__main__":
    if not os.path.isfile(util.FILE_TRAINING):
        print("%s file does not exist." % util.FILE_TRAINING)
        sys.exit()

    saved_data = util.load_var(util.FILE_TRAINING)

    # Reformat
    train_dataset, train_labels = reformat(saved_data["train"]["dataset"], saved_data["train"]["labels"])
    valid_dataset, valid_labels = reformat(saved_data["valid"]["dataset"], saved_data["valid"]["labels"])
    test_dataset, test_labels = reformat(saved_data["test"]["dataset"], saved_data["test"]["labels"])

    reformatted = {
        "train": {
            "dataset": train_dataset,
            "labels": train_labels
        },
        "valid": {
            "dataset": valid_dataset,
            "labels": valid_labels
        },
        "test": {
            "dataset": test_dataset,
            "labels": test_labels
        }
    }

    util.print_stats(reformatted)

    # Problem 00
    run_StochasticGradientDescentRELU_conv(reformatted)

'''
- Problem 1, with max pooling

+-------+---------------------+--------+-------+--------------+
|       |       tensor        |  mean  | stdev | label_tensor |
+=======+=====================+========+=======+==============+
| test  | (18724, 28, 28, 1)  | -0.075 | 0.459 | (18724, 10)  |
+-------+---------------------+--------+-------+--------------+
| train | (200000, 28, 28, 1) | -0.082 | 0.454 | (200000, 10) |
+-------+---------------------+--------+-------+--------------+
| valid | (10000, 28, 28, 1)  | -0.079 | 0.455 | (10000, 10)  |
+-------+---------------------+--------+-------+--------------+
### Running StochasticGradientDescentRELU
logits: (16, 10)
 ## Starting training sample using run_StochasticGradientDescentRELU_deep
Initialized
Minibatch loss at step 0: 2.485101
Minibatch accuracy: 25.0%
Validation accuracy: 10.1%
Minibatch loss at step 50: 1.756581
Minibatch accuracy: 31.2%
Validation accuracy: 39.5%
Minibatch loss at step 100: 1.079543
Minibatch accuracy: 62.5%
Validation accuracy: 72.0%
Minibatch loss at step 150: 0.989855
Minibatch accuracy: 68.8%
Validation accuracy: 72.9%
Minibatch loss at step 200: 1.280299
Minibatch accuracy: 75.0%
Validation accuracy: 74.9%
Minibatch loss at step 250: 0.941732
Minibatch accuracy: 75.0%
Validation accuracy: 74.9%
Minibatch loss at step 300: 0.893623
Minibatch accuracy: 68.8%
Validation accuracy: 79.2%
Minibatch loss at step 350: 1.012591
Minibatch accuracy: 75.0%
Validation accuracy: 78.5%
Minibatch loss at step 400: 0.631816
Minibatch accuracy: 81.2%
Validation accuracy: 81.5%
Minibatch loss at step 450: 0.733148
Minibatch accuracy: 81.2%
Validation accuracy: 76.5%
Minibatch loss at step 500: 0.598374
Minibatch accuracy: 81.2%
Validation accuracy: 82.4%
Minibatch loss at step 550: 0.640256
Minibatch accuracy: 75.0%
Validation accuracy: 82.5%
Minibatch loss at step 600: 0.848935
Minibatch accuracy: 68.8%
Validation accuracy: 82.1%
Minibatch loss at step 650: 0.978754
Minibatch accuracy: 68.8%
Validation accuracy: 83.3%
Minibatch loss at step 700: 0.145683
Minibatch accuracy: 100.0%
Validation accuracy: 83.3%
Minibatch loss at step 750: 0.461914
Minibatch accuracy: 87.5%
Validation accuracy: 84.0%
Minibatch loss at step 800: 0.587214
Minibatch accuracy: 87.5%
Validation accuracy: 84.1%
Minibatch loss at step 850: 0.401895
Minibatch accuracy: 81.2%
Validation accuracy: 82.9%
Minibatch loss at step 900: 0.361387
Minibatch accuracy: 87.5%
Validation accuracy: 83.9%
Minibatch loss at step 950: 0.323557
Minibatch accuracy: 93.8%
Validation accuracy: 82.7%
Minibatch loss at step 1000: 0.733815
Minibatch accuracy: 81.2%
Validation accuracy: 84.5%
Test accuracy: 90.7%
  - Training completed in 135.56 secs
'''



'''
- CODE INITIALIZATION

+-------+---------------------+--------+-------+--------------+
|       |       tensor        |  mean  | stdev | label_tensor |
+=======+=====================+========+=======+==============+
| test  | (18724, 28, 28, 1)  | -0.075 | 0.459 | (18724, 10)  |
+-------+---------------------+--------+-------+--------------+
| train | (200000, 28, 28, 1) | -0.082 | 0.454 | (200000, 10) |
+-------+---------------------+--------+-------+--------------+
| valid | (10000, 28, 28, 1)  | -0.079 | 0.455 | (10000, 10)  |
+-------+---------------------+--------+-------+--------------+
### Running StochasticGradientDescentRELU
logits: (16, 10)
 ## Starting training sample using run_StochasticGradientDescentRELU_deep
Initialized
Minibatch loss at step 0: 2.958678
Minibatch accuracy: 18.8%
Validation accuracy: 10.1%
Minibatch loss at step 50: 1.841308
Minibatch accuracy: 43.8%
Validation accuracy: 38.7%
Minibatch loss at step 100: 0.951090
Minibatch accuracy: 75.0%
Validation accuracy: 69.1%
Minibatch loss at step 150: 0.956349
Minibatch accuracy: 68.8%
Validation accuracy: 70.2%
Minibatch loss at step 200: 1.175654
Minibatch accuracy: 68.8%
Validation accuracy: 75.3%
Minibatch loss at step 250: 0.890618
Minibatch accuracy: 75.0%
Validation accuracy: 76.7%
Minibatch loss at step 300: 1.054653
Minibatch accuracy: 68.8%
Validation accuracy: 78.5%
Minibatch loss at step 350: 1.115427
Minibatch accuracy: 62.5%
Validation accuracy: 77.3%
Minibatch loss at step 400: 0.842588
Minibatch accuracy: 81.2%
Validation accuracy: 81.3%
Minibatch loss at step 450: 0.849736
Minibatch accuracy: 81.2%
Validation accuracy: 76.7%
Minibatch loss at step 500: 0.685506
Minibatch accuracy: 75.0%
Validation accuracy: 79.4%
Minibatch loss at step 550: 0.743250
Minibatch accuracy: 75.0%
Validation accuracy: 81.0%
Minibatch loss at step 600: 0.998314
Minibatch accuracy: 62.5%
Validation accuracy: 81.0%
Minibatch loss at step 650: 1.196464
Minibatch accuracy: 62.5%
Validation accuracy: 82.1%
Minibatch loss at step 700: 0.160754
Minibatch accuracy: 100.0%
Validation accuracy: 81.5%
Minibatch loss at step 750: 0.491822
Minibatch accuracy: 68.8%
Validation accuracy: 82.6%
Minibatch loss at step 800: 0.761056
Minibatch accuracy: 87.5%
Validation accuracy: 82.0%
Minibatch loss at step 850: 0.415586
Minibatch accuracy: 93.8%
Validation accuracy: 82.0%
Minibatch loss at step 900: 0.238108
Minibatch accuracy: 93.8%
Validation accuracy: 83.0%
Minibatch loss at step 950: 0.380422
Minibatch accuracy: 87.5%
Validation accuracy: 82.3%
Minibatch loss at step 1000: 0.851744
Minibatch accuracy: 75.0%
Validation accuracy: 83.3%
Test accuracy: 90.1%
  - Training completed in 37.29 secs
'''
