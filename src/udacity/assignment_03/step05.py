#!/usr/bin/env python

'''
SOURCE: https://github.com/tensorflow/tensorflow/blob/master/tensorflow/examples/udacity/3_regularization.ipynb
REF:
    * https://github.com/rringham/deep-learning-notebooks/blob/master/notMNIST_four_hidden_layers.ipynb
    * https://discussions.udacity.com/t/assignment-3-3-how-to-implement-dropout/45730/24
    * https://discussions.udacity.com/t/problem-3-3-dropout-does-not-improve-test-accuarcy/46286/15

Created for:
    * Assignment 3 - Problem 04

ToDo:
    * https://github.com/tensorflow/tensorflow/blob/master/tensorflow/tensorboard/README.md
'''

from __future__ import print_function

import os
import sys
import math
import numpy as np
import tensorflow as tf
from six.moves import range

from common import Util as util
from common import Timer

image_size = 28
num_labels = 10

LOG_DIR = "./log/"
LEARNING_RATE = 0.5
NUM_STEPS = 100001
BETA_REGULARIZATION = 0.001
LAYERS = {
    "output": [
        {"size": 1024},
        {"size": 1024},
        {"size": 305},
        {"size": 75}
    ]
}
DROPOUT = 0.5


def accuracy(predictions, labels):
    return (
        100.0 * np.sum(
            np.argmax(predictions, 1) == np.argmax(labels, 1)
        ) / predictions.shape[0]
    )


def build_layers(start_dataset, start_size, output_size, dataset_labels):
    dataset = start_dataset
    size = start_size

    # Create hidden layers
    for layer in LAYERS["output"]:
        layer_size = layer["size"]
        layer["weights"] = tf.Variable(
            tf.truncated_normal([size, layer_size], stddev=math.sqrt(2.0/size))
        )
        layer["biases"] = tf.Variable(tf.zeros([layer_size]))
        logit = tf.nn.relu(tf.add(tf.matmul(dataset, layer["weights"]), layer["biases"]))
        layer["logit"] = tf.nn.dropout(logit, DROPOUT)
        # layer["logit"] = logit

        size = layer_size
        dataset = layer["logit"]

    # Create output
    weights = tf.Variable(
        tf.truncated_normal([size, output_size], stddev=math.sqrt(2.0/size))
    )
    biases = tf.Variable(tf.zeros([output_size]))
    logits = tf.matmul(dataset, weights) + biases
    loss = tf.reduce_mean(
        tf.nn.softmax_cross_entropy_with_logits(logits, dataset_labels)
    )

    # Compute beta regularization
    # loss_r2 = loss + BETA_REGULARIZATION * tf.nn.l2_loss(weights)
    loss_r2_beta = 0
    for layer in LAYERS["output"]:
        loss_r2_beta += BETA_REGULARIZATION * tf.nn.l2_loss(layer["weights"])
    loss_r2 = loss + BETA_REGULARIZATION * tf.nn.l2_loss(weights) + loss_r2_beta

    return weights, biases, logits, loss_r2


def build_prediction(key, start_dataset, weights, biases):
    dataset = start_dataset
    LAYERS[key] = []
    # Build hidden
    for layer in LAYERS["output"]:
        obj = {}
        obj["hidden"] = tf.nn.relu(tf.add(tf.matmul(dataset, layer["weights"]), layer["biases"]))
        LAYERS[key].append(obj)
        dataset = obj["hidden"]

    # Build Prediction
    prediction = tf.nn.softmax(
        tf.matmul(dataset, weights) + biases
    )

    return prediction


def run_StochasticGradientDescentRELU_deep(reformatted):
    print("### Running StochasticGradientDescentRELU")
    train_dataset = reformatted["train"]["dataset"]
    train_labels = reformatted["train"]["labels"]
    valid_dataset = reformatted["valid"]["dataset"]
    valid_labels = reformatted["valid"]["labels"]
    test_dataset = reformatted["test"]["dataset"]
    test_labels = reformatted["test"]["labels"]

    batch_size = 128

    graph = tf.Graph()

    # with graph.as_default():

    # Define step variable
    tf_init_steps = tf.Variable(0, tf.int32)

    # Input data. For the training data, we use a placeholder that will be fed
    # at run time with a training minibatch.
    tf_train_dataset = tf.placeholder(tf.float32, shape=(batch_size, image_size * image_size))
    tf_train_labels = tf.placeholder(tf.float32, shape=(batch_size, num_labels))
    tf_valid_dataset = tf.constant(valid_dataset)
    tf_test_dataset = tf.constant(test_dataset)

    # Build layers
    weights, biases, logits, loss = build_layers(
        tf_train_dataset, image_size * image_size, num_labels, tf_train_labels
    )
    print("logits: %s" % logits.get_shape())

    # Applying l2_loss on just the final weight result in:
    # BETA of 0.001 produced 89.2%, virtually unchanged from expected 89.4%
    # BETA of 0.01 produced 88.6%%, worsen
    #
    # Applying l2_loss for all weights included in the hidden layer:
    # BETA of 0.001 produced 93.0%, a great improvement from 89.4%
    # BETA of 0.01 produced 89.7%, worsen

    # Optimizer
    global_step = tf.Variable(0)
    learn_rate = tf.train.exponential_decay(LEARNING_RATE, global_step, 1, 0.9999)
    optimizer = tf.train.GradientDescentOptimizer(learn_rate).minimize(loss, global_step=global_step)

    # Predictions for the training, validation, and test data.
    train_prediction = tf.nn.softmax(logits)

    # Validation
    valid_prediction = build_prediction("valid", tf_valid_dataset, weights, biases)

    # Test
    test_prediction = build_prediction("test", tf_test_dataset, weights, biases)

    print(" ## Starting training sample using run_StochasticGradientDescentRELU_deep")
    timer = Timer()
    saver = tf.train.Saver()

    with tf.Session() as session:
        tf.initialize_all_variables().run()

        ckpt = tf.train.get_checkpoint_state("%s/saver" % LOG_DIR)
        if ckpt:
            saver.restore(session, ckpt.model_checkpoint_path)
            print("Restored Session with step %s" % tf_init_steps.eval())
        else:
            tf.initialize_all_variables().run()
            print("Initialized with step: %s" % tf_init_steps.eval())

        print("Initialized")
        for step in range(tf_init_steps.eval(), NUM_STEPS):
            # Pick an offset within the training data, which has been randomized.
            # Note: we could use better randomization across epochs.
            offset = (step * batch_size) % (train_labels.shape[0] - batch_size)
            # Generate a minibatch.
            batch_data = train_dataset[offset:(offset + batch_size), :]
            batch_labels = train_labels[offset:(offset + batch_size), :]
            # Prepare a dictionary telling the session where to feed the minibatch.
            # The key of the dictionary is the placeholder node of the graph to be fed,
            # and the value is the numpy array to feed to it.
            feed_dict = {tf_train_dataset: batch_data, tf_train_labels: batch_labels}
            _, l, predictions = session.run(
                [optimizer, loss, train_prediction], feed_dict=feed_dict)
            if (step % 500 == 0):
                print("Minibatch loss at step %d: %f" % (step, l))
                print("Minibatch accuracy: %.1f%%" % accuracy(predictions, batch_labels))
                print("Validation accuracy: %.1f%%" % accuracy(
                    valid_prediction.eval(), valid_labels))
                tf_init_steps.assign(step)
                session.run(tf_init_steps)
                saver.save(session, "%s/saver" % LOG_DIR, global_step=step+1)
        print("Test accuracy: %.1f%%" % accuracy(test_prediction.eval(), test_labels))
    print("  - Training completed in %.2f secs" % timer.stop())

#
# RUN THE SCRIPT
#
if __name__ == "__main__":
    if not os.path.isfile(util.FILE_REFORMATTED):
        print("%s file does not exist.  Make sure to run step01.py" % util.FILE_TRAINING)
        sys.exit()

    reformatted = util.load_var(util.FILE_REFORMATTED)
    util.print_stats(reformatted)

    # Problem 03
    run_StochasticGradientDescentRELU_deep(reformatted)


'''
+-------+---------------+--------+-------+--------------+
|       |    tensor     |  mean  | stdev | label_tensor |
+=======+===============+========+=======+==============+
| test  | (18724, 784)  | -0.075 | 0.459 | (18724, 10)  |
+-------+---------------+--------+-------+--------------+
| train | (200000, 784) | -0.082 | 0.454 | (200000, 10) |
+-------+---------------+--------+-------+--------------+
| valid | (10000, 784)  | -0.079 | 0.455 | (10000, 10)  |
+-------+---------------+--------+-------+--------------+
### Running StochasticGradientDescentRELU
logits: (128, 10)
 ## Starting training sample using run_StochasticGradientDescentRELU_deep
Initialized with step: 0
Initialized
Minibatch loss at step 0: 5.175786
Minibatch accuracy: 5.5%
Validation accuracy: 11.2%
Minibatch loss at step 500: 1.906208
Minibatch accuracy: 81.2%
Validation accuracy: 83.1%
Minibatch loss at step 1000: 1.505927
Minibatch accuracy: 80.5%
Validation accuracy: 84.3%
Minibatch loss at step 1500: 1.213274
Minibatch accuracy: 82.0%
Validation accuracy: 85.3%
Minibatch loss at step 2000: 0.968368
Minibatch accuracy: 88.3%
Validation accuracy: 85.4%
Minibatch loss at step 2500: 0.901065
Minibatch accuracy: 85.9%
Validation accuracy: 86.3%
Minibatch loss at step 3000: 0.897284
Minibatch accuracy: 80.5%
Validation accuracy: 85.8%
Minibatch loss at step 3500: 0.818620
Minibatch accuracy: 84.4%
Validation accuracy: 86.5%
Minibatch loss at step 4000: 0.629002
Minibatch accuracy: 89.1%
Validation accuracy: 87.0%
Minibatch loss at step 4500: 0.684446
Minibatch accuracy: 85.2%
Validation accuracy: 86.8%
Minibatch loss at step 5000: 0.686745
Minibatch accuracy: 86.7%
Validation accuracy: 87.3%
Minibatch loss at step 5500: 0.589122
Minibatch accuracy: 88.3%
Validation accuracy: 87.3%
Minibatch loss at step 6000: 0.775565
Minibatch accuracy: 80.5%
Validation accuracy: 87.2%
Minibatch loss at step 6500: 0.641495
Minibatch accuracy: 88.3%
Validation accuracy: 87.2%
Minibatch loss at step 7000: 0.540336
Minibatch accuracy: 90.6%
Validation accuracy: 88.0%
Minibatch loss at step 7500: 0.558329
Minibatch accuracy: 85.9%
Validation accuracy: 87.9%
Minibatch loss at step 8000: 0.708688
Minibatch accuracy: 85.9%
Validation accuracy: 88.2%
Minibatch loss at step 8500: 0.564337
Minibatch accuracy: 88.3%
Validation accuracy: 88.2%
Minibatch loss at step 9000: 0.498606
Minibatch accuracy: 88.3%
Validation accuracy: 88.2%
Minibatch loss at step 9500: 0.574107
Minibatch accuracy: 86.7%
Validation accuracy: 88.3%
Minibatch loss at step 10000: 0.620480
Minibatch accuracy: 89.1%
Validation accuracy: 88.8%
Minibatch loss at step 10500: 0.503668
Minibatch accuracy: 89.8%
Validation accuracy: 88.3%
Minibatch loss at step 11000: 0.654828
Minibatch accuracy: 86.7%
Validation accuracy: 88.7%
Minibatch loss at step 11500: 0.594619
Minibatch accuracy: 86.7%
Validation accuracy: 88.7%
Minibatch loss at step 12000: 0.559806
Minibatch accuracy: 91.4%
Validation accuracy: 88.6%
Minibatch loss at step 12500: 0.493999
Minibatch accuracy: 88.3%
Validation accuracy: 89.1%
Minibatch loss at step 13000: 0.629810
Minibatch accuracy: 88.3%
Validation accuracy: 88.9%
Minibatch loss at step 13500: 0.484156
Minibatch accuracy: 89.1%
Validation accuracy: 89.0%
Minibatch loss at step 14000: 0.489375
Minibatch accuracy: 90.6%
Validation accuracy: 89.3%
Minibatch loss at step 14500: 0.645924
Minibatch accuracy: 84.4%
Validation accuracy: 88.9%
Minibatch loss at step 15000: 0.489739
Minibatch accuracy: 89.1%
Validation accuracy: 89.4%
Minibatch loss at step 15500: 0.459244
Minibatch accuracy: 92.2%
Validation accuracy: 89.5%
Minibatch loss at step 16000: 0.578577
Minibatch accuracy: 87.5%
Validation accuracy: 89.0%
Minibatch loss at step 16500: 0.556869
Minibatch accuracy: 90.6%
Validation accuracy: 89.2%
Minibatch loss at step 17000: 0.496666
Minibatch accuracy: 89.8%
Validation accuracy: 89.2%
Minibatch loss at step 17500: 0.507564
Minibatch accuracy: 89.8%
Validation accuracy: 89.4%
Minibatch loss at step 18000: 0.409391
Minibatch accuracy: 92.2%
Validation accuracy: 89.6%
Minibatch loss at step 18500: 0.536632
Minibatch accuracy: 89.1%
Validation accuracy: 89.6%
Minibatch loss at step 19000: 0.554738
Minibatch accuracy: 89.1%
Validation accuracy: 89.7%
Minibatch loss at step 19500: 0.513177
Minibatch accuracy: 87.5%
Validation accuracy: 90.0%
Minibatch loss at step 20000: 0.492348
Minibatch accuracy: 89.8%
Validation accuracy: 89.8%
Minibatch loss at step 20500: 0.516293
Minibatch accuracy: 89.8%
Validation accuracy: 89.9%
Minibatch loss at step 21000: 0.479314
Minibatch accuracy: 89.1%
Validation accuracy: 89.9%
Minibatch loss at step 21500: 0.456932
Minibatch accuracy: 92.2%
Validation accuracy: 90.1%
Minibatch loss at step 22000: 0.516128
Minibatch accuracy: 89.8%
Validation accuracy: 90.0%
Minibatch loss at step 22500: 0.530931
Minibatch accuracy: 89.1%
Validation accuracy: 90.1%
Minibatch loss at step 23000: 0.505888
Minibatch accuracy: 88.3%
Validation accuracy: 90.1%
Minibatch loss at step 23500: 0.424489
Minibatch accuracy: 92.2%
Validation accuracy: 90.0%
Minibatch loss at step 24000: 0.440260
Minibatch accuracy: 91.4%
Validation accuracy: 90.0%
Minibatch loss at step 24500: 0.381309
Minibatch accuracy: 93.8%
Validation accuracy: 90.1%
Minibatch loss at step 25000: 0.335119
Minibatch accuracy: 95.3%
Validation accuracy: 90.2%
Minibatch loss at step 25500: 0.493484
Minibatch accuracy: 90.6%
Validation accuracy: 90.3%
Minibatch loss at step 26000: 0.462645
Minibatch accuracy: 90.6%
Validation accuracy: 90.3%
Minibatch loss at step 26500: 0.454296
Minibatch accuracy: 87.5%
Validation accuracy: 90.4%
Minibatch loss at step 27000: 0.592455
Minibatch accuracy: 87.5%
Validation accuracy: 90.3%
Minibatch loss at step 27500: 0.575031
Minibatch accuracy: 88.3%
Validation accuracy: 90.4%
Minibatch loss at step 28000: 0.480609
Minibatch accuracy: 87.5%
Validation accuracy: 90.4%
Minibatch loss at step 28500: 0.507815
Minibatch accuracy: 87.5%
Validation accuracy: 90.5%
Minibatch loss at step 29000: 0.359019
Minibatch accuracy: 93.0%
Validation accuracy: 90.3%
Minibatch loss at step 29500: 0.513274
Minibatch accuracy: 89.8%
Validation accuracy: 90.6%
Minibatch loss at step 30000: 0.391130
Minibatch accuracy: 90.6%
Validation accuracy: 90.5%
Minibatch loss at step 30500: 0.429193
Minibatch accuracy: 91.4%
Validation accuracy: 90.5%
Minibatch loss at step 31000: 0.476422
Minibatch accuracy: 91.4%
Validation accuracy: 90.4%
Minibatch loss at step 31500: 0.488070
Minibatch accuracy: 89.8%
Validation accuracy: 90.5%
Minibatch loss at step 32000: 0.447068
Minibatch accuracy: 90.6%
Validation accuracy: 90.5%
Minibatch loss at step 32500: 0.412596
Minibatch accuracy: 91.4%
Validation accuracy: 90.5%
Minibatch loss at step 33000: 0.552472
Minibatch accuracy: 88.3%
Validation accuracy: 90.7%
Minibatch loss at step 33500: 0.321106
Minibatch accuracy: 94.5%
Validation accuracy: 90.8%
Minibatch loss at step 34000: 0.342392
Minibatch accuracy: 95.3%
Validation accuracy: 90.7%
Minibatch loss at step 34500: 0.473535
Minibatch accuracy: 90.6%
Validation accuracy: 90.7%
Minibatch loss at step 35000: 0.391810
Minibatch accuracy: 92.2%
Validation accuracy: 90.6%
Minibatch loss at step 35500: 0.375892
Minibatch accuracy: 91.4%
Validation accuracy: 90.6%
Minibatch loss at step 36000: 0.472257
Minibatch accuracy: 90.6%
Validation accuracy: 90.8%
Minibatch loss at step 36500: 0.373218
Minibatch accuracy: 90.6%
Validation accuracy: 90.6%
Minibatch loss at step 37000: 0.342237
Minibatch accuracy: 94.5%
Validation accuracy: 90.6%
Minibatch loss at step 37500: 0.524095
Minibatch accuracy: 89.8%
Validation accuracy: 90.8%
Minibatch loss at step 38000: 0.356535
Minibatch accuracy: 93.8%
Validation accuracy: 90.6%
Minibatch loss at step 38500: 0.350109
Minibatch accuracy: 95.3%
Validation accuracy: 90.7%
Minibatch loss at step 39000: 0.453766
Minibatch accuracy: 90.6%
Validation accuracy: 90.8%
Minibatch loss at step 39500: 0.444353
Minibatch accuracy: 91.4%
Validation accuracy: 90.6%
Minibatch loss at step 40000: 0.413964
Minibatch accuracy: 90.6%
Validation accuracy: 90.7%
Minibatch loss at step 40500: 0.371511
Minibatch accuracy: 93.8%
Validation accuracy: 90.7%
Minibatch loss at step 41000: 0.422258
Minibatch accuracy: 91.4%
Validation accuracy: 90.8%
Minibatch loss at step 41500: 0.435892
Minibatch accuracy: 90.6%
Validation accuracy: 90.7%
Minibatch loss at step 42000: 0.429662
Minibatch accuracy: 90.6%
Validation accuracy: 90.9%
Minibatch loss at step 42500: 0.287181
Minibatch accuracy: 94.5%
Validation accuracy: 90.8%
Minibatch loss at step 43000: 0.461635
Minibatch accuracy: 91.4%
Validation accuracy: 90.8%
Minibatch loss at step 43500: 0.410380
Minibatch accuracy: 90.6%
Validation accuracy: 90.8%
Minibatch loss at step 44000: 0.331642
Minibatch accuracy: 94.5%
Validation accuracy: 90.9%
Minibatch loss at step 44500: 0.304906
Minibatch accuracy: 96.1%
Validation accuracy: 90.8%
Minibatch loss at step 45000: 0.391492
Minibatch accuracy: 92.2%
Validation accuracy: 90.9%
Minibatch loss at step 45500: 0.475937
Minibatch accuracy: 90.6%
Validation accuracy: 90.9%
Minibatch loss at step 46000: 0.328547
Minibatch accuracy: 96.1%
Validation accuracy: 90.9%
Minibatch loss at step 46500: 0.328582
Minibatch accuracy: 93.8%
Validation accuracy: 91.0%
Minibatch loss at step 47000: 0.370986
Minibatch accuracy: 93.8%
Validation accuracy: 90.9%
Minibatch loss at step 47500: 0.442517
Minibatch accuracy: 88.3%
Validation accuracy: 90.8%
Minibatch loss at step 48000: 0.335873
Minibatch accuracy: 95.3%
Validation accuracy: 90.8%
Minibatch loss at step 48500: 0.411310
Minibatch accuracy: 91.4%
Validation accuracy: 90.9%
Minibatch loss at step 49000: 0.396370
Minibatch accuracy: 90.6%
Validation accuracy: 90.8%
Minibatch loss at step 49500: 0.309098
Minibatch accuracy: 94.5%
Validation accuracy: 90.8%
Minibatch loss at step 50000: 0.391773
Minibatch accuracy: 91.4%
Validation accuracy: 90.9%
Minibatch loss at step 50500: 0.509600
Minibatch accuracy: 87.5%
Validation accuracy: 90.8%
Minibatch loss at step 51000: 0.298915
Minibatch accuracy: 96.1%
Validation accuracy: 90.9%
Minibatch loss at step 51500: 0.372656
Minibatch accuracy: 93.0%
Validation accuracy: 90.9%
Minibatch loss at step 52000: 0.281935
Minibatch accuracy: 97.7%
Validation accuracy: 90.9%
Minibatch loss at step 52500: 0.467312
Minibatch accuracy: 88.3%
Validation accuracy: 90.9%
Minibatch loss at step 53000: 0.345952
Minibatch accuracy: 93.8%
Validation accuracy: 90.9%
Minibatch loss at step 53500: 0.509752
Minibatch accuracy: 89.1%
Validation accuracy: 90.9%
Minibatch loss at step 54000: 0.318595
Minibatch accuracy: 95.3%
Validation accuracy: 90.9%
Minibatch loss at step 54500: 0.549459
Minibatch accuracy: 87.5%
Validation accuracy: 90.9%
Minibatch loss at step 55000: 0.350882
Minibatch accuracy: 93.8%
Validation accuracy: 90.9%
Minibatch loss at step 55500: 0.361305
Minibatch accuracy: 92.2%
Validation accuracy: 90.9%
Minibatch loss at step 56000: 0.299294
Minibatch accuracy: 94.5%
Validation accuracy: 90.9%
Minibatch loss at step 56500: 0.391661
Minibatch accuracy: 94.5%
Validation accuracy: 90.9%
Minibatch loss at step 57000: 0.309291
Minibatch accuracy: 94.5%
Validation accuracy: 90.9%
Minibatch loss at step 57500: 0.273667
Minibatch accuracy: 96.9%
Validation accuracy: 90.9%
Minibatch loss at step 58000: 0.315669
Minibatch accuracy: 94.5%
Validation accuracy: 90.9%
Minibatch loss at step 58500: 0.432023
Minibatch accuracy: 89.1%
Validation accuracy: 90.9%
Minibatch loss at step 59000: 0.408325
Minibatch accuracy: 90.6%
Validation accuracy: 90.9%
Minibatch loss at step 59500: 0.303191
Minibatch accuracy: 96.1%
Validation accuracy: 90.9%
Minibatch loss at step 60000: 0.419293
Minibatch accuracy: 95.3%
Validation accuracy: 90.8%
Minibatch loss at step 60500: 0.402509
Minibatch accuracy: 92.2%
Validation accuracy: 90.9%
Minibatch loss at step 61000: 0.357265
Minibatch accuracy: 93.8%
Validation accuracy: 90.9%
Minibatch loss at step 61500: 0.347559
Minibatch accuracy: 92.2%
Validation accuracy: 90.9%
Minibatch loss at step 62000: 0.439014
Minibatch accuracy: 93.0%
Validation accuracy: 90.9%
Minibatch loss at step 62500: 0.530492
Minibatch accuracy: 87.5%
Validation accuracy: 90.9%
Minibatch loss at step 63000: 0.364957
Minibatch accuracy: 92.2%
Validation accuracy: 90.9%
Minibatch loss at step 63500: 0.310021
Minibatch accuracy: 94.5%
Validation accuracy: 90.9%
Minibatch loss at step 64000: 0.444146
Minibatch accuracy: 92.2%
Validation accuracy: 90.9%
Minibatch loss at step 64500: 0.341657
Minibatch accuracy: 95.3%
Validation accuracy: 90.9%
Minibatch loss at step 65000: 0.334546
Minibatch accuracy: 94.5%
Validation accuracy: 91.0%
Minibatch loss at step 65500: 0.501860
Minibatch accuracy: 88.3%
Validation accuracy: 90.9%
Minibatch loss at step 66000: 0.514511
Minibatch accuracy: 90.6%
Validation accuracy: 90.9%
Minibatch loss at step 66500: 0.465784
Minibatch accuracy: 89.1%
Validation accuracy: 90.9%
Minibatch loss at step 67000: 0.367576
Minibatch accuracy: 92.2%
Validation accuracy: 90.9%
Minibatch loss at step 67500: 0.471724
Minibatch accuracy: 89.8%
Validation accuracy: 90.9%
Minibatch loss at step 68000: 0.347205
Minibatch accuracy: 92.2%
Validation accuracy: 90.9%
Minibatch loss at step 68500: 0.332494
Minibatch accuracy: 93.0%
Validation accuracy: 90.9%
Minibatch loss at step 69000: 0.490992
Minibatch accuracy: 88.3%
Validation accuracy: 90.9%
Minibatch loss at step 69500: 0.406328
Minibatch accuracy: 91.4%
Validation accuracy: 90.9%
Minibatch loss at step 70000: 0.373786
Minibatch accuracy: 92.2%
Validation accuracy: 90.9%
Minibatch loss at step 70500: 0.507643
Minibatch accuracy: 87.5%
Validation accuracy: 90.9%
Minibatch loss at step 71000: 0.344083
Minibatch accuracy: 94.5%
Validation accuracy: 90.9%
Minibatch loss at step 71500: 0.469555
Minibatch accuracy: 92.2%
Validation accuracy: 90.9%
Minibatch loss at step 72000: 0.384033
Minibatch accuracy: 93.0%
Validation accuracy: 90.9%
Minibatch loss at step 72500: 0.337885
Minibatch accuracy: 93.8%
Validation accuracy: 90.9%
Minibatch loss at step 73000: 0.323884
Minibatch accuracy: 93.8%
Validation accuracy: 90.9%
Minibatch loss at step 73500: 0.437107
Minibatch accuracy: 89.8%
Validation accuracy: 90.9%
Minibatch loss at step 74000: 0.328871
Minibatch accuracy: 94.5%
Validation accuracy: 90.9%
Minibatch loss at step 74500: 0.403949
Minibatch accuracy: 93.0%
Validation accuracy: 90.9%
Minibatch loss at step 75000: 0.321481
Minibatch accuracy: 95.3%
Validation accuracy: 90.9%
Minibatch loss at step 75500: 0.469170
Minibatch accuracy: 88.3%
Validation accuracy: 90.9%
Minibatch loss at step 76000: 0.443880
Minibatch accuracy: 91.4%
Validation accuracy: 90.9%
Minibatch loss at step 76500: 0.422039
Minibatch accuracy: 90.6%
Validation accuracy: 90.9%
Minibatch loss at step 77000: 0.395809
Minibatch accuracy: 93.8%
Validation accuracy: 90.9%
Minibatch loss at step 77500: 0.340793
Minibatch accuracy: 94.5%
Validation accuracy: 90.9%
Minibatch loss at step 78000: 0.472521
Minibatch accuracy: 93.8%
Validation accuracy: 90.9%
Minibatch loss at step 78500: 0.485625
Minibatch accuracy: 90.6%
Validation accuracy: 90.9%
Minibatch loss at step 79000: 0.374758
Minibatch accuracy: 93.0%
Validation accuracy: 90.9%
Minibatch loss at step 79500: 0.432470
Minibatch accuracy: 92.2%
Validation accuracy: 90.9%
Minibatch loss at step 80000: 0.306261
Minibatch accuracy: 96.9%
Validation accuracy: 90.9%
Minibatch loss at step 80500: 0.543535
Minibatch accuracy: 87.5%
Validation accuracy: 90.9%
Minibatch loss at step 81000: 0.398259
Minibatch accuracy: 91.4%
Validation accuracy: 90.9%
Minibatch loss at step 81500: 0.321978
Minibatch accuracy: 93.8%
Validation accuracy: 90.9%
Minibatch loss at step 82000: 0.326954
Minibatch accuracy: 93.0%
Validation accuracy: 90.9%
Minibatch loss at step 82500: 0.310681
Minibatch accuracy: 94.5%
Validation accuracy: 90.9%
Minibatch loss at step 83000: 0.276832
Minibatch accuracy: 96.1%
Validation accuracy: 90.9%
Minibatch loss at step 83500: 0.426998
Minibatch accuracy: 89.8%
Validation accuracy: 90.9%
Minibatch loss at step 84000: 0.298284
Minibatch accuracy: 96.1%
Validation accuracy: 90.9%
Minibatch loss at step 84500: 0.415391
Minibatch accuracy: 93.0%
Validation accuracy: 90.9%
Minibatch loss at step 85000: 0.334120
Minibatch accuracy: 94.5%
Validation accuracy: 90.9%
Minibatch loss at step 85500: 0.402577
Minibatch accuracy: 93.0%
Validation accuracy: 90.9%
Minibatch loss at step 86000: 0.406194
Minibatch accuracy: 92.2%
Validation accuracy: 90.9%
Minibatch loss at step 86500: 0.323166
Minibatch accuracy: 95.3%
Validation accuracy: 90.9%
Minibatch loss at step 87000: 0.357448
Minibatch accuracy: 94.5%
Validation accuracy: 90.9%
Minibatch loss at step 87500: 0.316614
Minibatch accuracy: 96.1%
Validation accuracy: 90.9%
Minibatch loss at step 88000: 0.424475
Minibatch accuracy: 92.2%
Validation accuracy: 90.9%
Minibatch loss at step 88500: 0.426560
Minibatch accuracy: 92.2%
Validation accuracy: 90.9%
Minibatch loss at step 89000: 0.390892
Minibatch accuracy: 90.6%
Validation accuracy: 90.9%
Minibatch loss at step 89500: 0.449829
Minibatch accuracy: 89.1%
Validation accuracy: 90.9%
Minibatch loss at step 90000: 0.360917
Minibatch accuracy: 93.0%
Validation accuracy: 90.9%
Minibatch loss at step 90500: 0.347277
Minibatch accuracy: 93.0%
Validation accuracy: 90.9%
Minibatch loss at step 91000: 0.388808
Minibatch accuracy: 95.3%
Validation accuracy: 90.9%
Minibatch loss at step 91500: 0.358425
Minibatch accuracy: 93.0%
Validation accuracy: 90.9%
Minibatch loss at step 92000: 0.262666
Minibatch accuracy: 95.3%
Validation accuracy: 90.9%
Minibatch loss at step 92500: 0.383878
Minibatch accuracy: 93.0%
Validation accuracy: 90.9%
Minibatch loss at step 93000: 0.429233
Minibatch accuracy: 92.2%
Validation accuracy: 90.9%
Minibatch loss at step 93500: 0.314921
Minibatch accuracy: 93.8%
Validation accuracy: 90.9%
Minibatch loss at step 94000: 0.387918
Minibatch accuracy: 93.8%
Validation accuracy: 90.9%
Minibatch loss at step 94500: 0.358071
Minibatch accuracy: 93.0%
Validation accuracy: 90.9%
Minibatch loss at step 95000: 0.486787
Minibatch accuracy: 89.1%
Validation accuracy: 90.9%
Minibatch loss at step 95500: 0.477768
Minibatch accuracy: 89.8%
Validation accuracy: 90.9%
Minibatch loss at step 96000: 0.420775
Minibatch accuracy: 92.2%
Validation accuracy: 90.9%
Minibatch loss at step 96500: 0.385043
Minibatch accuracy: 93.0%
Validation accuracy: 90.9%
Minibatch loss at step 97000: 0.459650
Minibatch accuracy: 88.3%
Validation accuracy: 90.9%
Minibatch loss at step 97500: 0.416414
Minibatch accuracy: 89.8%
Validation accuracy: 90.9%
Minibatch loss at step 98000: 0.366877
Minibatch accuracy: 93.0%
Validation accuracy: 90.9%
Minibatch loss at step 98500: 0.439982
Minibatch accuracy: 92.2%
Validation accuracy: 90.9%
Minibatch loss at step 99000: 0.400174
Minibatch accuracy: 93.8%
Validation accuracy: 90.9%
Minibatch loss at step 99500: 0.475083
Minibatch accuracy: 91.4%
Validation accuracy: 90.9%
Minibatch loss at step 100000: 0.506636
Minibatch accuracy: 89.8%
Validation accuracy: 90.9%
Test accuracy: 96.2%
  - Training completed in 4772.85 secs
'''