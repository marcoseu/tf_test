#!/usr/bin/env python

'''
SOURCE: https://github.com/tensorflow/tensorflow/blob/master/tensorflow/examples/udacity/3_regularization.ipynb

Created for:
* Assignment 3 - Problem 02

Overfitting: Describe a model that too closely resemble the train dataset. 

By reducing the training data to 2k, with batch size of 1k, we essentially
ensure that the model fits the tranining data.  See result:

Initialized
Minibatch loss at step 0: 19.794067
Minibatch accuracy: 10.7%
Validation accuracy: 13.0%
Minibatch loss at step 500: 1.941520
Minibatch accuracy: 97.3%
Validation accuracy: 69.7%
Minibatch loss at step 1000: 1.159007
Minibatch accuracy: 99.6%
Validation accuracy: 71.9%
Minibatch loss at step 1500: 0.743590
Minibatch accuracy: 99.9%
Validation accuracy: 73.5%
Minibatch loss at step 2000: 0.498775
Minibatch accuracy: 99.9%
Validation accuracy: 74.9%
Minibatch loss at step 2500: 0.353018
Minibatch accuracy: 100.0%
Validation accuracy: 75.7%
Minibatch loss at step 3000: 0.266102
Minibatch accuracy: 100.0%
Validation accuracy: 76.2%
Test accuracy: 83.3%
  - Training completed in 23.12 secs
### Running StochasticGradientDescentRELU
logits: TensorShape([Dimension(1000), Dimension(10)])
 ## Starting training sample using run_StochasticGradientDescentRELU_deep
Initialized
Minibatch loss at step 0: 640.473022
Minibatch accuracy: 8.9%
Validation accuracy: 40.0%
Minibatch loss at step 500: 190.017761
Minibatch accuracy: 100.0%
Validation accuracy: 77.3%
Minibatch loss at step 1000: 115.238289
Minibatch accuracy: 100.0%
Validation accuracy: 77.2%
Minibatch loss at step 1500: 69.888062
Minibatch accuracy: 100.0%
Validation accuracy: 77.3%
Minibatch loss at step 2000: 42.381954
Minibatch accuracy: 100.0%
Validation accuracy: 77.4%
Minibatch loss at step 2500: 25.705196
Minibatch accuracy: 100.0%
Validation accuracy: 77.7%
Minibatch loss at step 3000: 15.592393
Minibatch accuracy: 100.0%
Validation accuracy: 77.8%
Test accuracy: 85.0%
  - Training completed in 443.72 secs

Note that Minibatch accuracy is at 100% but test accuracy is below

CONCLUSION:

It clear the overfitting due to 100% on minibatch.  Also, the Validation
accuracy reaches quickly to the max and stays at around 77% without much
improvement through-out the steps.  The test result is suprisingly not
greatly affected but it was lower than others (92% vs 85% for deep learing)

This is essentially caused by the model being too closely fitted with the
training dataset and therefore not ideal for other data.

'''

from __future__ import print_function

import os
import sys
import numpy as np
import tensorflow as tf
from six.moves import range

from common import Util as util
from common import Timer

image_size = 28
num_labels = 10

LEARNING_RATE = 0.5
NUM_STEPS = 3001
BETA_REGULARIZATION = 0.001
LAYERS = {
    "output": [
        {"size": 1024}
    ]
}


def accuracy(predictions, labels):
    return (
        100.0 * np.sum(
            np.argmax(predictions, 1) == np.argmax(labels, 1)
        ) / predictions.shape[0]
    )


def build_layers(start_dataset, start_size, output_size, dataset_labels):
    dataset = start_dataset
    size = start_size

    # Create hidden layers
    for layer in LAYERS["output"]:
        layer_size = layer["size"]
        layer["weights"] = tf.Variable(
            tf.truncated_normal([size, layer_size])
        )
        layer["biases"] = tf.Variable(tf.zeros([layer_size]))
        layer["logit"] = tf.nn.relu(tf.add(tf.matmul(dataset, layer["weights"]), layer["biases"]))

        size = layer_size
        dataset = layer["logit"]

    # Create output
    weights = tf.Variable(
        tf.truncated_normal([size, output_size]))
    biases = tf.Variable(tf.zeros([output_size]))
    logits = tf.matmul(dataset, weights) + biases
    loss = tf.reduce_mean(
        tf.nn.softmax_cross_entropy_with_logits(logits, dataset_labels)
    )

    # Compute beta regularization
    # loss_r2 = loss + BETA_REGULARIZATION * tf.nn.l2_loss(weights)
    loss_r2_beta = 0
    for layer in LAYERS["output"]:
        loss_r2_beta += BETA_REGULARIZATION * tf.nn.l2_loss(layer["weights"])
    loss_r2 = loss + BETA_REGULARIZATION * tf.nn.l2_loss(weights) + loss_r2_beta

    return weights, biases, logits, loss_r2


def build_prediction(key, start_dataset, weights, biases):
    dataset = start_dataset
    LAYERS[key] = []
    # Build hidden
    for layer in LAYERS["output"]:
        obj = {}
        obj["hidden"] = tf.nn.relu(tf.add(tf.matmul(dataset, layer["weights"]), layer["biases"]))
        LAYERS[key].append(obj)
        dataset = obj["hidden"]

    # Build Prediction
    prediction = tf.nn.softmax(
        tf.matmul(dataset, weights) + biases
    )

    return prediction


def run_StochasticGradientDescent(reformatted):
    print("### Running StochasticGradientDescent")
    train_dataset = reformatted["train"]["dataset"]
    train_labels = reformatted["train"]["labels"]
    valid_dataset = reformatted["valid"]["dataset"]
    valid_labels = reformatted["valid"]["labels"]
    test_dataset = reformatted["test"]["dataset"]
    test_labels = reformatted["test"]["labels"]

    batch_size = 1000

    graph = tf.Graph()
    with graph.as_default():

        # Input data. For the training data, we use a placeholder that will be fed
        # at run time with a training minibatch.
        tf_train_dataset = tf.placeholder(tf.float32, shape=(batch_size, image_size * image_size))
        tf_train_labels = tf.placeholder(tf.float32, shape=(batch_size, num_labels))
        tf_valid_dataset = tf.constant(valid_dataset)
        tf_test_dataset = tf.constant(test_dataset)

        # Variables.
        weights = tf.Variable(
            tf.truncated_normal([image_size * image_size, num_labels]))
        biases = tf.Variable(tf.zeros([num_labels]))

        # Training computation.
        logits = tf.matmul(tf_train_dataset, weights) + biases
        loss = tf.reduce_mean(
            tf.nn.softmax_cross_entropy_with_logits(logits, tf_train_labels)
        )
        loss_r2 = loss + BETA_REGULARIZATION * tf.nn.l2_loss(weights)
        # Best result is 88.1% using 0.001.  With 0.002, we got 88.2%

        # Optimizer.
        optimizer = tf.train.GradientDescentOptimizer(0.5).minimize(loss_r2)

        # Predictions for the training, validation, and test data.
        train_prediction = tf.nn.softmax(logits)
        valid_prediction = tf.nn.softmax(
            tf.matmul(tf_valid_dataset, weights) + biases
        )
        test_prediction = tf.nn.softmax(tf.matmul(tf_test_dataset, weights) + biases)

    # Note: This seems to run a different sample of 128 for 3001 times
    num_steps = 3001

    print(" ## Starting training sample using StochasticGradientDescent")
    timer = Timer()

    with tf.Session(graph=graph) as session:
        tf.initialize_all_variables().run()
        print("Initialized")
        for step in range(num_steps):
            # Pick an offset within the training data, which has been randomized.
            # Note: we could use better randomization across epochs.
            offset = (step * batch_size) % (train_labels.shape[0] - batch_size)
            # Generate a minibatch.
            batch_data = train_dataset[offset:(offset + batch_size), :]
            batch_labels = train_labels[offset:(offset + batch_size), :]
            # Prepare a dictionary telling the session where to feed the minibatch.
            # The key of the dictionary is the placeholder node of the graph to be fed,
            # and the value is the numpy array to feed to it.
            feed_dict = {tf_train_dataset: batch_data, tf_train_labels: batch_labels}
            _, l, predictions = session.run(
                [optimizer, loss_r2, train_prediction], feed_dict=feed_dict)
            if (step % 500 == 0):
                print("Minibatch loss at step %d: %f" % (step, l))
                print("Minibatch accuracy: %.1f%%" % accuracy(predictions, batch_labels))
                print("Validation accuracy: %.1f%%" % accuracy(
                    valid_prediction.eval(), valid_labels))
        print("Test accuracy: %.1f%%" % accuracy(test_prediction.eval(), test_labels))
    print("  - Training completed in %.2f secs" % timer.stop())


def run_StochasticGradientDescentRELU_deep(reformatted):
    print("### Running StochasticGradientDescentRELU")
    train_dataset = reformatted["train"]["dataset"]
    train_labels = reformatted["train"]["labels"]
    valid_dataset = reformatted["valid"]["dataset"]
    valid_labels = reformatted["valid"]["labels"]
    test_dataset = reformatted["test"]["dataset"]
    test_labels = reformatted["test"]["labels"]

    batch_size = 1000

    graph = tf.Graph()
    with graph.as_default():

        # Input data. For the training data, we use a placeholder that will be fed
        # at run time with a training minibatch.
        tf_train_dataset = tf.placeholder(tf.float32, shape=(batch_size, image_size * image_size))
        tf_train_labels = tf.placeholder(tf.float32, shape=(batch_size, num_labels))
        tf_valid_dataset = tf.constant(valid_dataset)
        tf_test_dataset = tf.constant(test_dataset)

        # Build layers
        weights, biases, logits, loss = build_layers(
            tf_train_dataset, image_size * image_size, num_labels, tf_train_labels
        )
        print("logits: %s" % logits.get_shape())

        # Applying l2_loss on just the final weight result in:
        # BETA of 0.001 produced 89.2%, virtually unchanged from expected 89.4%
        # BETA of 0.01 produced 88.6%%, worsen
        # 
        # Applying l2_loss for all weights included in the hidden layer:
        # BETA of 0.001 produced 93.0%, a great improvement from 89.4%
        # BETA of 0.01 produced 89.7%, worsen

        # Optimizer
        optimizer = tf.train.GradientDescentOptimizer(LEARNING_RATE).minimize(loss)

        # Predictions for the training, validation, and test data.
        train_prediction = tf.nn.softmax(logits)

        # Validation
        valid_prediction = build_prediction("valid", tf_valid_dataset, weights, biases)

        # Test
        test_prediction = build_prediction("test", tf_test_dataset, weights, biases)

    print(" ## Starting training sample using run_StochasticGradientDescentRELU_deep")
    timer = Timer()

    with tf.Session(graph=graph) as session:
        tf.initialize_all_variables().run()
        print("Initialized")
        for step in range(NUM_STEPS):
            # Pick an offset within the training data, which has been randomized.
            # Note: we could use better randomization across epochs.
            offset = (step * batch_size) % (train_labels.shape[0] - batch_size)
            # Generate a minibatch.
            batch_data = train_dataset[offset:(offset + batch_size), :]
            batch_labels = train_labels[offset:(offset + batch_size), :]
            # Prepare a dictionary telling the session where to feed the minibatch.
            # The key of the dictionary is the placeholder node of the graph to be fed,
            # and the value is the numpy array to feed to it.
            feed_dict = {tf_train_dataset: batch_data, tf_train_labels: batch_labels}
            _, l, predictions = session.run(
                [optimizer, loss, train_prediction], feed_dict=feed_dict)
            if (step % 500 == 0):
                print("Minibatch loss at step %d: %f" % (step, l))
                print("Minibatch accuracy: %.1f%%" % accuracy(predictions, batch_labels))
                print("Validation accuracy: %.1f%%" % accuracy(
                    valid_prediction.eval(), valid_labels))
        print("Test accuracy: %.1f%%" % accuracy(test_prediction.eval(), test_labels))
    print("  - Training completed in %.2f secs" % timer.stop())

#
# RUN THE SCRIPT
#
if __name__ == "__main__":
    if not os.path.isfile(util.FILE_REFORMATTED):
        print("%s file does not exist.  Make sure to run step01.py" % util.FILE_TRAINING)
        sys.exit()

    reformatted = util.load_var(util.FILE_REFORMATTED)

    # Reduce the size of train dataset
    SMALLER_SIZE = 2000
    reformatted["train"]["dataset"] = reformatted["train"]["dataset"][:SMALLER_SIZE, :]
    reformatted["train"]["labels"] = reformatted["train"]["labels"][:SMALLER_SIZE, :]

    util.print_stats(reformatted)

    # Problem 01
    run_StochasticGradientDescent(reformatted)
    run_StochasticGradientDescentRELU_deep(reformatted)

