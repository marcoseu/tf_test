Result using decay_steps of 2:
* learn_rate = tf.train.exponential_decay(LEARNING_RATE, global_step, 2, 0.9999)

+-------+---------------+--------+-------+--------------+
|       |    tensor     |  mean  | stdev | label_tensor |
+=======+===============+========+=======+==============+
| test  | (18724, 784)  | -0.075 | 0.459 | (18724, 10)  |
+-------+---------------+--------+-------+--------------+
| train | (200000, 784) | -0.082 | 0.454 | (200000, 10) |
+-------+---------------+--------+-------+--------------+
| valid | (10000, 784)  | -0.079 | 0.455 | (10000, 10)  |
+-------+---------------+--------+-------+--------------+
### Running StochasticGradientDescentRELU
logits: (128, 10)
 ## Starting training sample using run_StochasticGradientDescentRELU_deep
Initialized with step: 0
Initialized
Minibatch loss at step 0: 4.981321
Minibatch accuracy: 8.6%
Validation accuracy: 12.4%
Minibatch loss at step 500: 1.950770
Minibatch accuracy: 83.6%
Validation accuracy: 83.6%
Minibatch loss at step 1000: 1.720933
Minibatch accuracy: 78.1%
Validation accuracy: 83.9%
Minibatch loss at step 1500: 1.172734
Minibatch accuracy: 83.6%
Validation accuracy: 85.6%
Minibatch loss at step 2000: 0.913400
Minibatch accuracy: 88.3%
Validation accuracy: 85.7%
Minibatch loss at step 2500: 0.937279
Minibatch accuracy: 83.6%
Validation accuracy: 85.8%
Minibatch loss at step 3000: 0.886576
Minibatch accuracy: 82.8%
Validation accuracy: 85.7%
Minibatch loss at step 3500: 0.837152
Minibatch accuracy: 83.6%
Validation accuracy: 86.8%
Minibatch loss at step 4000: 0.635965
Minibatch accuracy: 88.3%
Validation accuracy: 86.2%
Minibatch loss at step 4500: 0.695161
Minibatch accuracy: 85.9%
Validation accuracy: 87.0%
Minibatch loss at step 5000: 0.595774
Minibatch accuracy: 89.1%
Validation accuracy: 87.0%
Minibatch loss at step 5500: 0.650479
Minibatch accuracy: 87.5%
Validation accuracy: 87.1%
Minibatch loss at step 6000: 0.753032
Minibatch accuracy: 85.2%
Validation accuracy: 86.9%
Minibatch loss at step 6500: 0.662636
Minibatch accuracy: 89.1%
Validation accuracy: 87.0%
Minibatch loss at step 7000: 0.598430
Minibatch accuracy: 88.3%
Validation accuracy: 86.2%
Minibatch loss at step 7500: 0.557169
Minibatch accuracy: 89.1%
Validation accuracy: 87.6%
Minibatch loss at step 8000: 0.727838
Minibatch accuracy: 82.8%
Validation accuracy: 87.5%
Minibatch loss at step 8500: 0.522499
Minibatch accuracy: 90.6%
Validation accuracy: 87.8%
Minibatch loss at step 9000: 0.578902
Minibatch accuracy: 88.3%
Validation accuracy: 87.1%
Minibatch loss at step 9500: 0.633981
Minibatch accuracy: 86.7%
Validation accuracy: 88.0%
Minibatch loss at step 10000: 0.704560
Minibatch accuracy: 87.5%
Validation accuracy: 88.0%
Minibatch loss at step 10500: 0.620746
Minibatch accuracy: 86.7%
Validation accuracy: 87.8%
Minibatch loss at step 11000: 0.703661
Minibatch accuracy: 86.7%
Validation accuracy: 87.7%
Minibatch loss at step 11500: 0.598892
Minibatch accuracy: 89.1%
Validation accuracy: 88.0%
Minibatch loss at step 12000: 0.744037
Minibatch accuracy: 82.8%
Validation accuracy: 88.1%
Minibatch loss at step 12500: 0.631750
Minibatch accuracy: 86.7%
Validation accuracy: 88.3%
Minibatch loss at step 13000: 0.663906
Minibatch accuracy: 84.4%
Validation accuracy: 88.3%
Minibatch loss at step 13500: 0.590753
Minibatch accuracy: 87.5%
Validation accuracy: 88.0%
Minibatch loss at step 14000: 0.565518
Minibatch accuracy: 88.3%
Validation accuracy: 88.2%
Minibatch loss at step 14500: 0.596333
Minibatch accuracy: 86.7%
Validation accuracy: 88.2%
Minibatch loss at step 15000: 0.566364
Minibatch accuracy: 89.1%
Validation accuracy: 88.9%
Minibatch loss at step 15500: 0.460614
Minibatch accuracy: 92.2%
Validation accuracy: 88.4%
Minibatch loss at step 16000: 0.681611
Minibatch accuracy: 87.5%
Validation accuracy: 88.4%
Minibatch loss at step 16500: 0.679144
Minibatch accuracy: 85.9%
Validation accuracy: 87.8%
Minibatch loss at step 17000: 0.640671
Minibatch accuracy: 84.4%
Validation accuracy: 88.6%
Minibatch loss at step 17500: 0.593396
Minibatch accuracy: 85.9%
Validation accuracy: 88.7%
Minibatch loss at step 18000: 0.483704
Minibatch accuracy: 91.4%
Validation accuracy: 88.4%
Minibatch loss at step 18500: 0.664860
Minibatch accuracy: 85.2%
Validation accuracy: 88.7%
Minibatch loss at step 19000: 0.591122
Minibatch accuracy: 86.7%
Validation accuracy: 88.5%
Minibatch loss at step 19500: 0.553095
Minibatch accuracy: 87.5%
Validation accuracy: 89.1%
Minibatch loss at step 20000: 0.594441
Minibatch accuracy: 87.5%
Validation accuracy: 88.7%
Minibatch loss at step 20500: 0.544439
Minibatch accuracy: 89.8%
Validation accuracy: 89.1%
Minibatch loss at step 21000: 0.634780
Minibatch accuracy: 85.2%
Validation accuracy: 88.7%
Minibatch loss at step 21500: 0.570715
Minibatch accuracy: 90.6%
Validation accuracy: 89.0%
Minibatch loss at step 22000: 0.549121
Minibatch accuracy: 89.1%
Validation accuracy: 89.0%
Minibatch loss at step 22500: 0.643014
Minibatch accuracy: 87.5%
Validation accuracy: 89.0%
Minibatch loss at step 23000: 0.579965
Minibatch accuracy: 85.9%
Validation accuracy: 89.1%
Minibatch loss at step 23500: 0.534379
Minibatch accuracy: 87.5%
Validation accuracy: 89.1%
Minibatch loss at step 24000: 0.526219
Minibatch accuracy: 89.1%
Validation accuracy: 89.4%
Minibatch loss at step 24500: 0.436635
Minibatch accuracy: 92.2%
Validation accuracy: 89.4%
Minibatch loss at step 25000: 0.515320
Minibatch accuracy: 89.8%
Validation accuracy: 89.5%
Minibatch loss at step 25500: 0.583730
Minibatch accuracy: 86.7%
Validation accuracy: 89.5%
Minibatch loss at step 26000: 0.556171
Minibatch accuracy: 86.7%
Validation accuracy: 89.5%
Minibatch loss at step 26500: 0.520716
Minibatch accuracy: 89.8%
Validation accuracy: 89.4%
Minibatch loss at step 27000: 0.624254
Minibatch accuracy: 86.7%
Validation accuracy: 89.4%
Minibatch loss at step 27500: 0.604090
Minibatch accuracy: 88.3%
Validation accuracy: 89.5%
Minibatch loss at step 28000: 0.506104
Minibatch accuracy: 88.3%
Validation accuracy: 89.5%
Minibatch loss at step 28500: 0.569631
Minibatch accuracy: 90.6%
Validation accuracy: 89.5%
Minibatch loss at step 29000: 0.411039
Minibatch accuracy: 92.2%
Validation accuracy: 89.8%
Minibatch loss at step 29500: 0.611691
Minibatch accuracy: 86.7%
Validation accuracy: 89.8%
Minibatch loss at step 30000: 0.508390
Minibatch accuracy: 87.5%
Validation accuracy: 89.6%
Minibatch loss at step 30500: 0.503084
Minibatch accuracy: 90.6%
Validation accuracy: 89.6%
Minibatch loss at step 31000: 0.605254
Minibatch accuracy: 89.1%
Validation accuracy: 89.6%
Minibatch loss at step 31500: 0.536574
Minibatch accuracy: 88.3%
Validation accuracy: 89.5%
Minibatch loss at step 32000: 0.563470
Minibatch accuracy: 87.5%
Validation accuracy: 89.6%
Minibatch loss at step 32500: 0.417130
Minibatch accuracy: 93.0%
Validation accuracy: 89.7%
Minibatch loss at step 33000: 0.583426
Minibatch accuracy: 89.1%
Validation accuracy: 89.9%
Minibatch loss at step 33500: 0.403949
Minibatch accuracy: 91.4%
Validation accuracy: 89.7%
Minibatch loss at step 34000: 0.389639
Minibatch accuracy: 93.8%
Validation accuracy: 90.0%
Minibatch loss at step 34500: 0.486458
Minibatch accuracy: 89.8%
Validation accuracy: 89.8%
Minibatch loss at step 35000: 0.415878
Minibatch accuracy: 91.4%
Validation accuracy: 89.9%
Minibatch loss at step 35500: 0.414469
Minibatch accuracy: 92.2%
Validation accuracy: 89.7%
Minibatch loss at step 36000: 0.633969
Minibatch accuracy: 87.5%
Validation accuracy: 90.0%
Minibatch loss at step 36500: 0.449869
Minibatch accuracy: 91.4%
Validation accuracy: 90.0%
Minibatch loss at step 37000: 0.468394
Minibatch accuracy: 90.6%
Validation accuracy: 90.2%
Minibatch loss at step 37500: 0.501208
Minibatch accuracy: 89.1%
Validation accuracy: 90.1%
Minibatch loss at step 38000: 0.437704
Minibatch accuracy: 90.6%
Validation accuracy: 90.1%
Minibatch loss at step 38500: 0.339075
Minibatch accuracy: 95.3%
Validation accuracy: 90.0%
Minibatch loss at step 39000: 0.531239
Minibatch accuracy: 87.5%
Validation accuracy: 90.3%
Minibatch loss at step 39500: 0.469822
Minibatch accuracy: 92.2%
Validation accuracy: 90.3%
Minibatch loss at step 40000: 0.562334
Minibatch accuracy: 87.5%
Validation accuracy: 90.1%
Minibatch loss at step 40500: 0.343365
Minibatch accuracy: 93.0%
Validation accuracy: 90.3%
Minibatch loss at step 41000: 0.530307
Minibatch accuracy: 89.1%
Validation accuracy: 90.4%
Minibatch loss at step 41500: 0.463241
Minibatch accuracy: 89.8%
Validation accuracy: 90.4%
Minibatch loss at step 42000: 0.514429
Minibatch accuracy: 90.6%
Validation accuracy: 90.3%
Minibatch loss at step 42500: 0.376183
Minibatch accuracy: 93.0%
Validation accuracy: 90.2%
Minibatch loss at step 43000: 0.438187
Minibatch accuracy: 90.6%
Validation accuracy: 90.4%
Minibatch loss at step 43500: 0.483429
Minibatch accuracy: 87.5%
Validation accuracy: 90.4%
Minibatch loss at step 44000: 0.366136
Minibatch accuracy: 94.5%
Validation accuracy: 90.5%
Minibatch loss at step 44500: 0.331008
Minibatch accuracy: 94.5%
Validation accuracy: 90.5%
Minibatch loss at step 45000: 0.465947
Minibatch accuracy: 90.6%
Validation accuracy: 90.4%
Minibatch loss at step 45500: 0.510863
Minibatch accuracy: 88.3%
Validation accuracy: 90.4%
Minibatch loss at step 46000: 0.365146
Minibatch accuracy: 91.4%
Validation accuracy: 90.3%
Minibatch loss at step 46500: 0.427626
Minibatch accuracy: 91.4%
Validation accuracy: 90.6%
Minibatch loss at step 47000: 0.401620
Minibatch accuracy: 91.4%
Validation accuracy: 90.5%
Minibatch loss at step 47500: 0.493323
Minibatch accuracy: 90.6%
Validation accuracy: 90.7%
Minibatch loss at step 48000: 0.323337
Minibatch accuracy: 94.5%
Validation accuracy: 90.6%
Minibatch loss at step 48500: 0.436281
Minibatch accuracy: 92.2%
Validation accuracy: 90.6%
Minibatch loss at step 49000: 0.416969
Minibatch accuracy: 92.2%
Validation accuracy: 90.6%
Minibatch loss at step 49500: 0.300839
Minibatch accuracy: 95.3%
Validation accuracy: 90.7%
Minibatch loss at step 50000: 0.442112
Minibatch accuracy: 91.4%
Validation accuracy: 90.6%
Minibatch loss at step 50500: 0.590483
Minibatch accuracy: 85.9%
Validation accuracy: 90.5%
Minibatch loss at step 51000: 0.275379
Minibatch accuracy: 96.1%
Validation accuracy: 90.5%
Minibatch loss at step 51500: 0.409446
Minibatch accuracy: 93.0%
Validation accuracy: 90.8%
Minibatch loss at step 52000: 0.310398
Minibatch accuracy: 96.9%
Validation accuracy: 90.5%
Minibatch loss at step 52500: 0.453099
Minibatch accuracy: 89.8%
Validation accuracy: 90.7%
Minibatch loss at step 53000: 0.419285
Minibatch accuracy: 91.4%
Validation accuracy: 90.6%
Minibatch loss at step 53500: 0.484739
Minibatch accuracy: 90.6%
Validation accuracy: 90.8%
Minibatch loss at step 54000: 0.344546
Minibatch accuracy: 95.3%
Validation accuracy: 90.7%
Minibatch loss at step 54500: 0.497816
Minibatch accuracy: 90.6%
Validation accuracy: 90.8%
Minibatch loss at step 55000: 0.380278
Minibatch accuracy: 92.2%
Validation accuracy: 90.8%
Minibatch loss at step 55500: 0.383544
Minibatch accuracy: 92.2%
Validation accuracy: 90.8%
Minibatch loss at step 56000: 0.442950
Minibatch accuracy: 90.6%
Validation accuracy: 90.8%
Minibatch loss at step 56500: 0.397346
Minibatch accuracy: 93.0%
Validation accuracy: 91.0%
Minibatch loss at step 57000: 0.371883
Minibatch accuracy: 95.3%
Validation accuracy: 90.7%
Minibatch loss at step 57500: 0.241950
Minibatch accuracy: 97.7%
Validation accuracy: 90.7%
Minibatch loss at step 58000: 0.341246
Minibatch accuracy: 93.0%
Validation accuracy: 90.9%
Minibatch loss at step 58500: 0.473887
Minibatch accuracy: 88.3%
Validation accuracy: 90.7%
Minibatch loss at step 59000: 0.523514
Minibatch accuracy: 89.1%
Validation accuracy: 91.0%
Minibatch loss at step 59500: 0.314853
Minibatch accuracy: 94.5%
Validation accuracy: 90.8%
Minibatch loss at step 60000: 0.387801
Minibatch accuracy: 93.0%
Validation accuracy: 90.9%
Minibatch loss at step 60500: 0.380268
Minibatch accuracy: 92.2%
Validation accuracy: 90.9%
Minibatch loss at step 61000: 0.389196
Minibatch accuracy: 93.0%
Validation accuracy: 91.0%
Minibatch loss at step 61500: 0.338949
Minibatch accuracy: 94.5%
Validation accuracy: 90.9%
Minibatch loss at step 62000: 0.445880
Minibatch accuracy: 93.0%
Validation accuracy: 90.9%
Minibatch loss at step 62500: 0.558110
Minibatch accuracy: 89.1%
Validation accuracy: 91.0%
Minibatch loss at step 63000: 0.402065
Minibatch accuracy: 90.6%
Validation accuracy: 91.0%
Minibatch loss at step 63500: 0.323424
Minibatch accuracy: 93.8%
Validation accuracy: 91.0%
Minibatch loss at step 64000: 0.424759
Minibatch accuracy: 91.4%
Validation accuracy: 91.0%
Minibatch loss at step 64500: 0.394189
Minibatch accuracy: 92.2%
Validation accuracy: 90.9%
Minibatch loss at step 65000: 0.317414
Minibatch accuracy: 96.1%
Validation accuracy: 90.9%
Minibatch loss at step 65500: 0.485817
Minibatch accuracy: 89.1%
Validation accuracy: 91.0%
Minibatch loss at step 66000: 0.479679
Minibatch accuracy: 90.6%
Validation accuracy: 90.9%
Minibatch loss at step 66500: 0.463875
Minibatch accuracy: 89.1%
Validation accuracy: 91.2%
Minibatch loss at step 67000: 0.384655
Minibatch accuracy: 93.8%
Validation accuracy: 91.0%
Minibatch loss at step 67500: 0.520039
Minibatch accuracy: 87.5%
Validation accuracy: 91.0%
Minibatch loss at step 68000: 0.344407
Minibatch accuracy: 93.0%
Validation accuracy: 91.0%
Minibatch loss at step 68500: 0.359027
Minibatch accuracy: 95.3%
Validation accuracy: 91.1%
Minibatch loss at step 69000: 0.455499
Minibatch accuracy: 90.6%
Validation accuracy: 91.0%
Minibatch loss at step 69500: 0.389285
Minibatch accuracy: 92.2%
Validation accuracy: 91.1%
Minibatch loss at step 70000: 0.360014
Minibatch accuracy: 92.2%
Validation accuracy: 91.0%
Minibatch loss at step 70500: 0.502705
Minibatch accuracy: 91.4%
Validation accuracy: 91.0%
Minibatch loss at step 71000: 0.329216
Minibatch accuracy: 95.3%
Validation accuracy: 91.1%
Minibatch loss at step 71500: 0.453121
Minibatch accuracy: 92.2%
Validation accuracy: 91.2%
Minibatch loss at step 72000: 0.360077
Minibatch accuracy: 93.8%
Validation accuracy: 90.9%
Minibatch loss at step 72500: 0.346412
Minibatch accuracy: 93.8%
Validation accuracy: 91.0%
Minibatch loss at step 73000: 0.373435
Minibatch accuracy: 93.8%
Validation accuracy: 91.2%
Minibatch loss at step 73500: 0.473934
Minibatch accuracy: 90.6%
Validation accuracy: 90.9%
Minibatch loss at step 74000: 0.331687
Minibatch accuracy: 95.3%
Validation accuracy: 91.2%
Minibatch loss at step 74500: 0.382546
Minibatch accuracy: 93.8%
Validation accuracy: 91.0%
Minibatch loss at step 75000: 0.389997
Minibatch accuracy: 93.0%
Validation accuracy: 91.1%
Minibatch loss at step 75500: 0.487909
Minibatch accuracy: 89.8%
Validation accuracy: 91.1%
Minibatch loss at step 76000: 0.439274
Minibatch accuracy: 92.2%
Validation accuracy: 91.1%
Minibatch loss at step 76500: 0.397448
Minibatch accuracy: 93.0%
Validation accuracy: 91.1%
Minibatch loss at step 77000: 0.323889
Minibatch accuracy: 95.3%
Validation accuracy: 91.0%
Minibatch loss at step 77500: 0.444736
Minibatch accuracy: 90.6%
Validation accuracy: 91.0%
Minibatch loss at step 78000: 0.467207
Minibatch accuracy: 90.6%
Validation accuracy: 91.2%
Minibatch loss at step 78500: 0.439913
Minibatch accuracy: 90.6%
Validation accuracy: 91.2%
Minibatch loss at step 79000: 0.382283
Minibatch accuracy: 93.8%
Validation accuracy: 91.2%
Minibatch loss at step 79500: 0.423039
Minibatch accuracy: 92.2%
Validation accuracy: 91.2%
Minibatch loss at step 80000: 0.342208
Minibatch accuracy: 94.5%
Validation accuracy: 91.2%
Minibatch loss at step 80500: 0.536659
Minibatch accuracy: 89.1%
Validation accuracy: 91.2%
Minibatch loss at step 81000: 0.445391
Minibatch accuracy: 89.1%
Validation accuracy: 91.3%
Minibatch loss at step 81500: 0.309160
Minibatch accuracy: 94.5%
Validation accuracy: 91.2%
Minibatch loss at step 82000: 0.318927
Minibatch accuracy: 95.3%
Validation accuracy: 91.1%
Minibatch loss at step 82500: 0.277420
Minibatch accuracy: 97.7%
Validation accuracy: 91.2%
Minibatch loss at step 83000: 0.283861
Minibatch accuracy: 96.1%
Validation accuracy: 91.2%
Minibatch loss at step 83500: 0.401575
Minibatch accuracy: 93.8%
Validation accuracy: 91.1%
Minibatch loss at step 84000: 0.279874
Minibatch accuracy: 95.3%
Validation accuracy: 91.2%
Minibatch loss at step 84500: 0.428324
Minibatch accuracy: 90.6%
Validation accuracy: 91.3%
Minibatch loss at step 85000: 0.329007
Minibatch accuracy: 94.5%
Validation accuracy: 91.1%
Minibatch loss at step 85500: 0.365899
Minibatch accuracy: 93.8%
Validation accuracy: 91.3%
Minibatch loss at step 86000: 0.388726
Minibatch accuracy: 92.2%
Validation accuracy: 91.2%
Minibatch loss at step 86500: 0.308882
Minibatch accuracy: 96.1%
Validation accuracy: 91.2%
Minibatch loss at step 87000: 0.325583
Minibatch accuracy: 94.5%
Validation accuracy: 91.2%
Minibatch loss at step 87500: 0.289191
Minibatch accuracy: 96.9%
Validation accuracy: 91.1%
Minibatch loss at step 88000: 0.478806
Minibatch accuracy: 92.2%
Validation accuracy: 91.2%
Minibatch loss at step 88500: 0.303853
Minibatch accuracy: 94.5%
Validation accuracy: 91.3%
Minibatch loss at step 89000: 0.445974
Minibatch accuracy: 90.6%
Validation accuracy: 91.3%
Minibatch loss at step 89500: 0.454614
Minibatch accuracy: 88.3%
Validation accuracy: 91.2%
Minibatch loss at step 90000: 0.393356
Minibatch accuracy: 91.4%
Validation accuracy: 91.2%
Minibatch loss at step 90500: 0.300547
Minibatch accuracy: 95.3%
Validation accuracy: 91.3%
Minibatch loss at step 91000: 0.409643
Minibatch accuracy: 93.8%
Validation accuracy: 91.3%
Minibatch loss at step 91500: 0.359866
Minibatch accuracy: 93.8%
Validation accuracy: 91.3%
Minibatch loss at step 92000: 0.303052
Minibatch accuracy: 94.5%
Validation accuracy: 91.4%
Minibatch loss at step 92500: 0.350454
Minibatch accuracy: 93.8%
Validation accuracy: 91.3%
Minibatch loss at step 93000: 0.431885
Minibatch accuracy: 90.6%
Validation accuracy: 91.3%
Minibatch loss at step 93500: 0.301232
Minibatch accuracy: 94.5%
Validation accuracy: 91.3%
Minibatch loss at step 94000: 0.328253
Minibatch accuracy: 95.3%
Validation accuracy: 91.3%
Minibatch loss at step 94500: 0.351775
Minibatch accuracy: 94.5%
Validation accuracy: 91.3%
Minibatch loss at step 95000: 0.441977
Minibatch accuracy: 89.1%
Validation accuracy: 91.4%
Minibatch loss at step 95500: 0.474661
Minibatch accuracy: 92.2%
Validation accuracy: 91.3%
Minibatch loss at step 96000: 0.365511
Minibatch accuracy: 93.8%
Validation accuracy: 91.4%
Minibatch loss at step 96500: 0.432908
Minibatch accuracy: 91.4%
Validation accuracy: 91.4%
Minibatch loss at step 97000: 0.432474
Minibatch accuracy: 89.8%
Validation accuracy: 91.4%
Minibatch loss at step 97500: 0.414988
Minibatch accuracy: 91.4%
Validation accuracy: 91.3%
Minibatch loss at step 98000: 0.373607
Minibatch accuracy: 93.0%
Validation accuracy: 91.4%
Minibatch loss at step 98500: 0.479402
Minibatch accuracy: 89.8%
Validation accuracy: 91.4%
Minibatch loss at step 99000: 0.407919
Minibatch accuracy: 93.8%
Validation accuracy: 91.3%
Minibatch loss at step 99500: 0.413439
Minibatch accuracy: 91.4%
Validation accuracy: 91.4%
Minibatch loss at step 100000: 0.471184
Minibatch accuracy: 89.1%
Validation accuracy: 91.4%
Test accuracy: 96.4%
  - Training completed in 4871.22 secs

