#!/usr/bin/env python

'''
Trying to understand what following does to an image:

image_data = (ndimage.imread(image_file).astype(float) - pixel_depth / 2) / pixel_depth

Result:

The formal above doesnt change the image.  Instead, it convert an integer between 0..255
to a floating number between -0.5..+0.5
'''

from __future__ import print_function

import os
import re

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from scipy import ndimage


def get_files(in_path, patterns=None):
    '''
    Recursively list all files from the path provided
    '''
    for path, dirs, files in os.walk(in_path):
        for file in files:
            file_name = os.path.join(path, file)
            if patterns:
                if patterns.search(file_name):
                    yield file_name
            else:
                yield file_name
        for dir in dirs:
            dir_name = os.path.join(path, dir)
            get_files(dir_name)


#
# RUN THE SCRIPT
#
if __name__ == "__main__":
    pixel_depth = 255.0  # Number of levels per pixel.

    def get_image_transform(input):
        return (input - pixel_depth / 2) / pixel_depth

    # Create Figure with Title
    fig = plt.figure()
    title = fig.suptitle("Blank Images")
    color_map = "Greys"
    blank_img = np.zeros((28, 28))

    # Create a blank initial image
    plt.subplot(1, 2, 1)
    # First image source is png so the range is 0 to 255
    im1 = plt.imshow(blank_img, cmap=color_map, vmin=0., vmax=255.)
    plt.subplot(1, 2, 2)
    # Second image get updated by get_image_transform
    im2 = plt.imshow(blank_img, cmap=color_map, vmin=-0.5, vmax=0.5)

    # Define image update
    def updateImage(png_file):
        title.set_text(os.path.basename(png_file))
        img = ndimage.imread(png_file).astype(float)
        im1.set_data(img)
        im2.set_data(get_image_transform(img))

    # Define animation
    ani = animation.FuncAnimation(
        fig, updateImage,
        frames=get_files("./data/notMNIST_small/", re.compile('\.png$')),
        interval=100, save_count=0
    )

    # Display image
    plt.show()
