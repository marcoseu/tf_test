#!/usr/bin/env python

'''
SOURCE: https://github.com/tensorflow/tensorflow/blob/master/tensorflow/examples/udacity/1_notmnist.ipynb

Created for:
* problem01
'''

from __future__ import print_function

import numpy as np
import os
import sys
from scipy import ndimage

from common import Util as util


def problem01():
    print("### PROBLEM 01")
    print("  - Loading raw image files into nparray")
    train_folders = util.get_directories(util.get_data_path("notMNIST_large/"))
    # train_folders = [util.get_data_path("notMNIST_large/E")]

    test_folders = util.get_directories(util.get_data_path("notMNIST_small/"))
    # test_folders = [util.get_data_path("notMNIST_small/E")]

    image_size = 28  # Pixel width and height.
    pixel_depth = 255.0  # Number of levels per pixel.

    def load(data_folders, min_num_images, max_num_images):
        dataset = np.ndarray(
            shape=(max_num_images, image_size, image_size), dtype=np.float32
        )
        labels = np.ndarray(shape=(max_num_images), dtype=np.int32)
        label_index = 0
        image_index = 0
        for folder in data_folders:
            print(folder)
            for image in os.listdir(folder):
                if image_index >= max_num_images:
                    raise Exception('More images than expected: %d >= %d' % (image_index, max_num_images))
                image_file = os.path.join(folder, image)
                try:
                    image_data = (ndimage.imread(image_file).astype(float) - pixel_depth / 2) / pixel_depth
                    if image_data.shape != (image_size, image_size):
                        raise Exception('Unexpected image shape: %s' % str(image_data.shape))
                    dataset[image_index, :, :] = image_data
                    labels[image_index] = label_index
                    image_index += 1
                except IOError as e:
                    print('Could not read:', image_file, ':', e, '- it\'s ok, skipping.')
            label_index += 1

        num_images = image_index
        dataset = dataset[0:num_images, :, :]
        labels = labels[0:num_images]
        if num_images < min_num_images:
            raise Exception('Many fewer images than expected: %d < %d' % (num_images, min_num_images))

        stat = util.compute_stat(dataset, labels)
        util.print_stat(stat)

        return dataset, labels, stat

    train_dataset, train_labels, train_stat = load(train_folders, 450000, 550000)
    test_dataset, test_labels, test_stat = load(test_folders, 18000, 20000)
    '''
    train_dataset, train_labels = load(train_folders, 0, 550000)
    test_dataset, test_labels = load(test_folders, 0, 20000)
    '''

    return {
        "train": {
            "dataset": train_dataset,
            "labels": train_labels
        },
        "test": {
            "dataset": test_dataset,
            "labels": test_labels
        }
    }


#
# RUN THE SCRIPT
#
if __name__ == "__main__":

    if os.path.isfile(util.FILE_ORIG):
        print("%s file already exists.  No need to run." % util.FILE_ORIG)
        sys.exit()

    result = problem01()

    print("  - Saving image nparray to %s" % util.FILE_ORIG)
    util.save_var(result, util.FILE_ORIG)

    util.print_stats(result)

'''
### RESULT
  - Saving image nparray to ./output/notMNIST_orig.hickle.gz
+-------+------------------+--------+-------+--------------+
|       |      tensor      |  mean  | stdev | label_tensor |
+=======+==================+========+=======+==============+
| test  | (18724, 28, 28)  | -0.075 | 0.459 | (18724,)     |
+-------+------------------+--------+-------+--------------+
| train | (529114, 28, 28) | -0.082 | 0.454 | (529114,)    |
+-------+------------------+--------+-------+--------------+
'''
