#!/usr/bin/env python

'''
SOURCE: https://github.com/tensorflow/tensorflow/blob/master/tensorflow/examples/udacity/1_notmnist.ipynb

Created for:
* problem06

'''

from __future__ import print_function
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import numpy as np
import os
import sys
from sklearn.linear_model import LogisticRegression
from sklearn import metrics
from itertools import chain

from common import Util as util
from common import Timer


def train_model(data, target):
    print(" ## Training model with sample size of %s", str(data.shape))
    timer = Timer()
    model = LogisticRegression()
    model.fit(data, target)
    print("  - Model:\n", model)
    print("  - Training completed in %.2f seconds" % timer.stop())
    return model


def make_prediction(model, data, target):
    print(" ## Making prediction for input size of %s", str(data.shape))
    timer = Timer()
    # make predictions
    expected = target
    predicted = model.predict(data)
    # summarize the fit of the model
    print(metrics.classification_report(expected, predicted))
    # print(metrics.confusion_matrix(expected, predicted))
    print("  - Prediction completed in %.2f seconds" % timer.stop())


def problem06(result, sample_size):
    print("### PROBLEM 06")

    train_dataset = result["train"]["dataset"]
    train_labels = result["train"]["labels"]
    valid_dataset = result["valid"]["dataset"]
    valid_labels = result["valid"]["labels"]
    test_dataset = result["test"]["dataset"]
    test_labels = result["test"]["labels"]

    # Train.
    train_data = train_dataset[:sample_size, :, :].reshape(sample_size, 28*28)
    train_target = train_labels[:sample_size]
    model = train_model(train_data, train_target)

    # Predict
    make_prediction(model, test_dataset.reshape(test_dataset.shape[0], 28*28), test_labels)

    # Return model
    return model


def show_prediction(model, data):
    dataset = data['dataset']
    labels = data['labels']

    # Create a generator that return one item at a time from data
    img_generator = chain(dataset)
    label_generator = chain(labels)

    # Create a image with N number of prediction in it
    img_rows = 3
    img_cols = 3
    fig = plt.figure()
    fig.suptitle("Press any key to show next predictions")
    im_range = range(img_rows * img_cols)

    # Initialize with blank images
    tls = []
    ims = []
    for i in im_range:
        sub = plt.subplot(img_rows, img_cols, i+1)
        sub.get_xaxis().set_visible(False)
        sub.get_yaxis().set_visible(False)
        tls.append(plt.title("Title %d" % i))
        ims.append(plt.imshow(np.zeros((28, 28)), cmap="Greys", vmin=-0.5, vmax=0.5))

    def updateImage(idx):
        # For some bizzard reason, the 0 idx is repeated twice hence missing the first page
        if idx > 0:
            plt.waitforbuttonpress()

        for i in im_range:
            img = next(img_generator)
            label = next(label_generator)
            # Using reshape(1, -1) to remove the error message
            #   validation.py:386: DeprecationWarning: Passing 1d arrays as data is deprecated in 0.17 and willraise ValueError
            #   in 0.19. Reshape your data either using X.reshape(-1, 1) if your data has a single feature or X.reshape(1, -1)
            #   if it contains a single sample.
            p = model.predict(img.reshape(28*28).reshape(1, -1))
            if p == label:
                p_text = "Correct: %s" % util.LABELS[p]
                color = "green"
            else:
                p_text = "%s should be %s" % (util.LABELS[p], util.LABELS[label])
                color = "red"
            print("[%d] %s" % (idx, p_text))
            tls[i].set_text(p_text)
            tls[i].set_color(color)
            ims[i].set_data(img)

    ani = animation.FuncAnimation(fig, updateImage, frames=xrange(dataset.shape[0]), repeat=False)
    plt.show()


#
# RUN THE SCRIPT
#
if __name__ == "__main__":
    if not os.path.isfile(util.FILE_TRAINING):
        print("%s file does not exist.  Make sure to run step02.py" % util.FILE_TRAINING)
        sys.exit()

    timer = Timer()
    print("### Loading previously saved data from %s" % util.FILE_TRAINING)
    result = util.load_var(util.FILE_TRAINING)
    print("  - Data loaded in %.2f seconds" % timer.stop())
    util.print_stats(result)

    # Train in 0.05s with avg precision of 0.66
    problem06(result, 50)

    # Train in 0.10s with avg precision of 0.76
    problem06(result, 100)

    # Train in 1.91s with avg precision of 0.84
    problem06(result, 1000)

    # Train in 16.97s with avg precision of 0.85
    model = problem06(result, 5000)

    # Show the result of prediction visually using the last trained model
    show_prediction(model, result['test'])
