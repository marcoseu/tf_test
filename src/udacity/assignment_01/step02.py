#!/usr/bin/env python

'''
SOURCE: https://github.com/tensorflow/tensorflow/blob/master/tensorflow/examples/udacity/1_notmnist.ipynb

Created for:
* problem02
* problem03
* problem04
'''
from __future__ import print_function

import matplotlib.pyplot as plt
import numpy as np
import os
import sys
from texttable import Texttable

from common import Util as util
from common import Timer


def showLabelDistribution(labels):
    # Sample every 1000th item of train_labels and plot the result
    y_data = labels[::1000]
    x_data = np.arange(y_data.size)
    plt.scatter(x_data, y_data)
    plt.show()


def showImage(dataset):
    '''
    Show first 4 images of the dataset
    '''
    for i in range(4):
        plt.subplot(2, 2, i+1)
        img = dataset[i, :, :]
        plt.imshow(img)
    plt.show()


# Randomize the data
def problem02(result):
    train_dataset = result["train"]["dataset"]
    train_labels = result["train"]["labels"]
    test_dataset = result["test"]["dataset"]
    test_labels = result["test"]["labels"]

    print("### PROBLEM 02")
    '''
    Let's verify that the data still looks good. Displaying a sample of the labels and images from the ndarray.
    Hint: you can use matplotlib.pyplot
    '''
    print(" ## Expect the label to be distributed sequencially")
    showLabelDistribution(train_labels)
    print("  - Show first 4 images")
    showImage(train_dataset)

    '''
    Next, we'll randomize the data. It's important to have the labels well shuffled for the training and test
    distributions to match
    '''
    print(" ## Shuffling data")

    np.random.seed(133)

    def randomize(dataset, labels):
        permutation = np.random.permutation(labels.shape[0])
        shuffled_dataset = dataset[permutation, :, :]
        shuffled_labels = labels[permutation]
        return shuffled_dataset, shuffled_labels

    shuffled_train_dataset, shuffled_train_labels = randomize(train_dataset, train_labels)
    shuffled_test_dataset, shuffled_test_labels = randomize(test_dataset, test_labels)

    # Return shuffled dataset and labels
    return {
        "train": {
            "dataset": shuffled_train_dataset,
            "labels": shuffled_train_labels
        },
        "test": {
            "dataset": shuffled_test_dataset,
            "labels": shuffled_test_labels
        }
    }


def problem03(shuffled, result):
    '''
    Convince yourself that the data is still good after shuffling!
    '''
    print("### PROBLEM 03")

    train_dataset = result["train"]["dataset"]
    train_labels = result["train"]["labels"]

    shuffled_train_dataset = shuffled["train"]["dataset"]
    shuffled_train_labels = shuffled["train"]["labels"]

    print(" ## Comparing original vs shuffled stats")

    o_stat = util.compute_stat(train_dataset, train_labels)
    s_stat = util.compute_stat(shuffled_train_dataset, shuffled_train_labels)

    tbl = Texttable()
    tbl.set_cols_align(["l", "r", "r"])
    rows = [["key", "orig", "shuffled"]]
    for key in o_stat:
        rows.append([key, o_stat[key], s_stat.get(key)])
    tbl.add_rows(rows)
    print(tbl.draw())


def problem04(shuffled):
    print("### PROBLEM 04")

    # The input result should be shuffled dataset
    train_dataset = shuffled["train"]["dataset"]
    train_labels = shuffled["train"]["labels"]
    test_dataset = shuffled["test"]["dataset"]
    test_labels = shuffled["test"]["labels"]

    '''
    Another check: we expect the data to be balanced across classes. Verify that.
    '''
    print(" ## Expect the label to be randomly distributed")
    showLabelDistribution(train_labels)
    print("  - Show first 4 images after shuffle")
    showImage(train_dataset)

    # Take a subset of data for validation
    print(" ## Create validation dataset")

    train_size = 200000
    valid_size = 10000

    valid_dataset = train_dataset[:valid_size, :, :]
    valid_labels = train_labels[:valid_size]
    train_dataset = train_dataset[valid_size:valid_size+train_size, :, :]
    train_labels = train_labels[valid_size:valid_size+train_size]
    print('Training', train_dataset.shape, train_labels.shape)
    print('Validation', valid_dataset.shape, valid_labels.shape)

    # Save the result so other script can continue
    return {
        "train": {
            "dataset": train_dataset,
            "labels": train_labels
        },
        "valid": {
            "dataset": valid_dataset,
            "labels": valid_labels
        },
        "test": {
            "dataset": test_dataset,
            "labels": test_labels
        }
    }


#
# RUN THE SCRIPT
#
if __name__ == "__main__":

    if not os.path.isfile(util.FILE_ORIG):
        print("%s file does not exist.  Make sure to run step01.py" % util.FILE_ORIG)
        sys.exit()

    # Read the save result from step01
    print("### Loading previously saved datasets from %s" % util.FILE_ORIG)
    timer = Timer()
    result = util.load_var(util.FILE_ORIG)
    print("  - Data loaded in %.2f seconds with stat" % timer.stop())
    util.print_stats(result)

    # Problem 2 -- If shuffled dataset already exists, use that
    if os.path.isfile(util.FILE_TRAINING):
        print("### Loading previously saved shuffled dataset from %s" % util.FILE_TRAINING)
        timer = Timer()
        shuffled = util.load_var(util.FILE_TRAINING)
        print("  - Data loaded in %.2f seconds with stat" % timer.stop())
        util.print_stats(result)
        save_shuffled = False
    else:
        shuffled = problem02(result)
        save_shuffled = True

    # Problem 3
    problem03(shuffled, result)

    if save_shuffled:
        # Problem 4
        training = problem04(shuffled)
        print("  - Saving training dataset to %s" % util.FILE_TRAINING)
        util.save_var(training, util.FILE_TRAINING)
        util.print_stats(training)

'''
+-------+------------------+--------+-------+--------------+
|       |      tensor      |  mean  | stdev | label_tensor |
+=======+==================+========+=======+==============+
| test  | (18724, 28, 28)  | -0.075 | 0.459 | (18724,)     |
+-------+------------------+--------+-------+--------------+
| train | (200000, 28, 28) | -0.082 | 0.454 | (200000,)    |
+-------+------------------+--------+-------+--------------+
| valid | (10000, 28, 28)  | -0.079 | 0.455 | (10000,)     |
+-------+------------------+--------+-------+--------------+
'''
