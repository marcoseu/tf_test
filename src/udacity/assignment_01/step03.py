#!/usr/bin/env python

'''
SOURCE: https://github.com/tensorflow/tensorflow/blob/master/tensorflow/examples/udacity/1_notmnist.ipynb

Created for:
* problem05
'''

from __future__ import print_function
import sys
import os
import hashlib
from texttable import Texttable

from common import Util as util
from common import Timer


def build_sha1_set(dataset):
    '''
    Return a set of hash of images
    '''
    result = set()
    for img in dataset:
        h = hashlib.sha256(img)
        result.add(h.digest())
    return result


def problem05(result):
    print("### PROBLEM 05")

    train_dataset = result["train"]["dataset"]
    train_labels = result["train"]["labels"]
    valid_dataset = result["valid"]["dataset"]
    valid_labels = result["valid"]["labels"]
    test_dataset = result["test"]["dataset"]
    test_labels = result["test"]["labels"]

    '''
    By construction, this dataset might contain a lot of overlapping samples, including training data
    that's also contained in the validation and test set! Overlap between training and test can skew
    the results if you expect to use your model in an environment where there is never an overlap, but are
    actually ok if you expect to see training samples recur when you use it. Measure how much overlap there
    is between training, validation and test samples.
    '''
    print("  - building sets")
    total_count = {
        "train": train_dataset.shape[0],
        "valid": valid_dataset.shape[0],
        "test": test_dataset.shape[0]
    }
    hash_sets = {
        "train": build_sha1_set(train_dataset),
        "valid": build_sha1_set(valid_dataset),
        "test": build_sha1_set(test_dataset)
    }
    keys = ["train", "test", "valid"]
    header = [""] + keys
    header_range = range(1, 4)

    tbl = Texttable()

    total_row = ["Total Items"]
    for x in header_range:
        header_key = header[x]
        total_row.append(total_count[header_key])

    hash_count = ["Total Hash"]
    for x in header_range:
        header_key = header[x]
        hash_count.append(len(hash_sets[header_key]))

    rows = [header, total_row, hash_count]
    for key in keys:
        row = [key]
        for x in header_range:
            header_key = header[x]
            if header_key == key:
                row.append("")
            else:
                '''
                print("Comparing set %s to %s" % (key, header_key))
                print("%s size %d" % (key, len(hash_sets[key])))
                print("%s size %d" % (header_key, len(hash_sets[header_key])))
                '''
                itsc = hash_sets[key].intersection(hash_sets[header_key])
                row.append(len(itsc))
        rows.append(row)
    tbl.add_rows(rows)
    print(tbl.draw())

    print ('''
The difference between the "Total Items" and "Total Hash" would be the number
of similar pictures.  This is result of reducing the image into a 28 x 28 matrix,
therefore reducing the accuracy of the original pictures
''')


#
# RUN THE SCRIPT
#
if __name__ == "__main__":
    if not os.path.isfile(util.FILE_TRAINING):
        print("%s file does not exist.  Make sure to run step02.py" % util.FILE_TRAINING)
        sys.exit()

    print("### Loading previously saved data from %s" % util.FILE_TRAINING)
    timer = Timer()
    result = util.load_var(util.FILE_TRAINING)
    print("  - Data loaded in %.2f seconds with stat" % timer.stop())
    util.print_stats(result)

    timer = Timer()
    problem05(result)
    print("  - stat computed in %.2f secs" % timer.stop())

