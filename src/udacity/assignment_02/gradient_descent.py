#!/usr/bin/env python

from __future__ import print_function

import os
import sys
import numpy as np
import tensorflow as tf
from six.moves import range

from common import Util as util
from common import Timer


image_size = 28
num_labels = 10


def accuracy(predictions, labels):
    return (
        100.0 * np.sum(
            np.argmax(predictions, 1) == np.argmax(labels, 1)
        ) / predictions.shape[0]
    )


def run_GradientDescentOptimizer(reformatted):
    print("### Running GradientDescentOptimizer")
    train_dataset = reformatted["train"]["dataset"]
    train_labels = reformatted["train"]["labels"]
    valid_dataset = reformatted["valid"]["dataset"]
    valid_labels = reformatted["valid"]["labels"]
    test_dataset = reformatted["test"]["dataset"]
    test_labels = reformatted["test"]["labels"]

    # With gradient descent training, even this much data is prohibitive.
    # Subset the training data for faster turnaround.
    #train_subset = 10000
    train_subset = 1000

    graph = tf.Graph()
    with graph.as_default():

        # Input data.
        # Load the training, validation and test data into constants that are
        # attached to the graph.
        tf_train_dataset = tf.constant(train_dataset[:train_subset, :])
        tf_train_labels = tf.constant(train_labels[:train_subset])
        tf_valid_dataset = tf.constant(valid_dataset)
        tf_test_dataset = tf.constant(test_dataset)

        # Variables.
        # These are the parameters that we are going to be training. The weight
        # matrix will be initialized using random valued following a (truncated)
        # normal distribution. The biases get initialized to zero.
        weights = tf.Variable(
            tf.truncated_normal([image_size * image_size, num_labels])
        )
        biases = tf.Variable(tf.zeros([num_labels]))

        # Training computation.
        # We multiply the inputs with the weight matrix, and add biases. We compute
        # the softmax and cross-entropy (it's one operation in TensorFlow, because
        # it's very common, and it can be optimized). We take the average of this
        # cross-entropy across all training examples: that's our loss.
        logits = tf.matmul(tf_train_dataset, weights) + biases
        loss = tf.reduce_mean(
            tf.nn.softmax_cross_entropy_with_logits(logits, tf_train_labels)
        )

        # Print the shape of the variables
        print("### Variable shapes")
        print(" - tf_train_dataset", tf_train_dataset.get_shape())
        print(" - tf_train_labels", tf_train_labels.get_shape())
        print(" - weights", weights.get_shape())
        print(" - biases", biases.get_shape())
        print(" - logits", logits.get_shape())
        print(" - loss", loss.get_shape())
        '''
         - tf_train_dataset TensorShape([Dimension(1000), Dimension(784)])
         - tf_train_labels TensorShape([Dimension(1000), Dimension(10)])
         - weights TensorShape([Dimension(784), Dimension(10)])
         - biases TensorShape([Dimension(10)])
         - logits TensorShape([Dimension(1000), Dimension(10)])
         - loss TensorShape([])
        '''

        # Optimizer.
        # We are going to find the minimum of this loss using gradient descent.
        optimizer = tf.train.GradientDescentOptimizer(0.5).minimize(loss)

        # Predictions for the training, validation, and test data.
        # These are not part of training, but merely here so that we can report
        # accuracy figures as we train.
        train_prediction = tf.nn.softmax(logits)
        valid_prediction = tf.nn.softmax(
            tf.matmul(tf_valid_dataset, weights) + biases
        )
        test_prediction = tf.nn.softmax(tf.matmul(tf_test_dataset, weights) + biases)

    num_steps = 801

    print(" ## Starting training sample of %d using GradientDescentOptimizer" % train_subset)
    timer = Timer()
    with tf.Session(graph=graph) as session:
        # This is a one-time operation which ensures the parameters get initialized as
        # we described in the graph: random weights for the matrix, zeros for the
        # biases.
        tf.initialize_all_variables().run()
        print('Initialized')
        for step in range(num_steps):
            # Run the computations. We tell .run() that we want to run the optimizer,
            # and get the loss value and the training predictions returned as numpy
            # arrays.
            _, l, predictions = session.run([optimizer, loss, train_prediction])
            if (step % 100 == 0):
                print('Loss at step %d: %f' % (step, l))
                print('Training accuracy: %.1f%%' % accuracy(
                    predictions, train_labels[:train_subset, :])
                )
                # Calling .eval() on valid_prediction is basically like calling run(), but
                # just to get that one numpy array. Note that it recomputes all its graph
                # dependencies.
                print('Validation accuracy: %.1f%%' % accuracy(
                    valid_prediction.eval(), valid_labels)
                )
        print('Test accuracy: %.1f%%' % accuracy(test_prediction.eval(), test_labels))
    print("  - Training completed in %.2f secs" % timer.stop())

#
# RUN THE SCRIPT
#
if __name__ == "__main__":

    if os.path.isfile(util.FILE_REFORMATTED):
        print("### Loading reformatted data from %s" % util.FILE_REFORMATTED)
        reformatted = util.load_var(util.FILE_REFORMATTED)
    else:
        print("%s file does not exist.  Make sure to run step01.py" % util.FILE_REFORMATTED)
        sys.exit()

    run_GradientDescentOptimizer(reformatted)
