#!/usr/bin/env python

'''
SOURCE: https://github.com/tensorflow/tensorflow/blob/master/tensorflow/examples/udacity/1_notmnist.ipynb

Created for:
* Assignment 2
'''

from __future__ import print_function

import os
import sys
import numpy as np
import tensorflow as tf
from six.moves import range

from common import Util as util
from common import Timer


image_size = 28
num_labels = 10

def reformat_result(result):
    print("### Re-formatting result")
    train_dataset = result["train"]["dataset"]
    train_labels = result["train"]["labels"]
    valid_dataset = result["valid"]["dataset"]
    valid_labels = result["valid"]["labels"]
    test_dataset = result["test"]["dataset"]
    test_labels = result["test"]["labels"]

    def reformat(dataset, labels):
        dataset = dataset.reshape((-1, image_size * image_size)).astype(np.float32)
        # Map 0 to [1.0, 0.0, 0.0 ...], 1 to [0.0, 1.0, 0.0 ...]
        labels = (np.arange(num_labels) == labels[:, None]).astype(np.float32)
        return dataset, labels

    train_dataset, train_labels = reformat(train_dataset, train_labels)
    valid_dataset, valid_labels = reformat(valid_dataset, valid_labels)
    test_dataset, test_labels = reformat(test_dataset, test_labels)

    return {
        "train": {
            "dataset": train_dataset,
            "labels": train_labels
        },
        "valid": {
            "dataset": valid_dataset,
            "labels": valid_labels
        },
        "test": {
            "dataset": test_dataset,
            "labels": test_labels
        }
    }


def accuracy(predictions, labels):
    return (
        100.0 * np.sum(
            np.argmax(predictions, 1) == np.argmax(labels, 1)
        ) / predictions.shape[0]
    )


def run_GradientDescentOptimizer(reformatted):
    print("### Running GradientDescentOptimizer")
    train_dataset = reformatted["train"]["dataset"]
    train_labels = reformatted["train"]["labels"]
    valid_dataset = reformatted["valid"]["dataset"]
    valid_labels = reformatted["valid"]["labels"]
    test_dataset = reformatted["test"]["dataset"]
    test_labels = reformatted["test"]["labels"]

    # With gradient descent training, even this much data is prohibitive.
    # Subset the training data for faster turnaround.
    train_subset = 10000

    graph = tf.Graph()
    with graph.as_default():

        # Input data.
        # Load the training, validation and test data into constants that are
        # attached to the graph.
        tf_train_dataset = tf.constant(train_dataset[:train_subset, :])
        tf_train_labels = tf.constant(train_labels[:train_subset])
        tf_valid_dataset = tf.constant(valid_dataset)
        tf_test_dataset = tf.constant(test_dataset)

        # Variables.
        # These are the parameters that we are going to be training. The weight
        # matrix will be initialized using random valued following a (truncated)
        # normal distribution. The biases get initialized to zero.
        weights = tf.Variable(
            tf.truncated_normal([image_size * image_size, num_labels])
        )
        biases = tf.Variable(tf.zeros([num_labels]))

        # Training computation.
        # We multiply the inputs with the weight matrix, and add biases. We compute
        # the softmax and cross-entropy (it's one operation in TensorFlow, because
        # it's very common, and it can be optimized). We take the average of this
        # cross-entropy across all training examples: that's our loss.
        logits = tf.matmul(tf_train_dataset, weights) + biases
        loss = tf.reduce_mean(
            tf.nn.softmax_cross_entropy_with_logits(logits, tf_train_labels)
        )

        # Optimizer.
        # We are going to find the minimum of this loss using gradient descent.
        optimizer = tf.train.GradientDescentOptimizer(0.5).minimize(loss)

        # Predictions for the training, validation, and test data.
        # These are not part of training, but merely here so that we can report
        # accuracy figures as we train.
        train_prediction = tf.nn.softmax(logits)
        valid_prediction = tf.nn.softmax(
            tf.matmul(tf_valid_dataset, weights) + biases
        )
        test_prediction = tf.nn.softmax(tf.matmul(tf_test_dataset, weights) + biases)

    # Note: It seems that all 10,000 training sample are ran through GradientDescentOptimizer
    #       for 801 times with a learning rate of 0.5
    num_steps = 801

    print(" ## Starting training sample of %d using GradientDescentOptimizer" % train_subset)
    timer = Timer()
    with tf.Session(graph=graph) as session:
        # This is a one-time operation which ensures the parameters get initialized as
        # we described in the graph: random weights for the matrix, zeros for the
        # biases.
        tf.initialize_all_variables().run()
        print('Initialized')
        for step in range(num_steps):
            # Run the computations. We tell .run() that we want to run the optimizer,
            # and get the loss value and the training predictions returned as numpy
            # arrays.
            _, l, predictions = session.run([optimizer, loss, train_prediction])
            # print("Predictions for %d: %s" % (l, predictions.shape))
            if (step % 100 == 0):
                print('Loss at step %d: %f' % (step, l))
                print('Training accuracy: %.1f%%' % accuracy(
                    predictions, train_labels[:train_subset, :])
                )
                # Calling .eval() on valid_prediction is basically like calling run(), but
                # just to get that one numpy array. Note that it recomputes all its graph
                # dependencies.
                print('Validation accuracy: %.1f%%' % accuracy(
                    valid_prediction.eval(), valid_labels)
                )
        print('Test accuracy: %.1f%%' % accuracy(test_prediction.eval(), test_labels))
    print("  - Training completed in %.2f secs" % timer.stop())


def run_StochasticGradientDescent(reformatted):
    print("### Running StochasticGradientDescent")
    train_dataset = reformatted["train"]["dataset"]
    train_labels = reformatted["train"]["labels"]
    valid_dataset = reformatted["valid"]["dataset"]
    valid_labels = reformatted["valid"]["labels"]
    test_dataset = reformatted["test"]["dataset"]
    test_labels = reformatted["test"]["labels"]

    batch_size = 128

    graph = tf.Graph()
    with graph.as_default():

        # Input data. For the training data, we use a placeholder that will be fed
        # at run time with a training minibatch.
        tf_train_dataset = tf.placeholder(tf.float32, shape=(batch_size, image_size * image_size))
        tf_train_labels = tf.placeholder(tf.float32, shape=(batch_size, num_labels))
        tf_valid_dataset = tf.constant(valid_dataset)
        tf_test_dataset = tf.constant(test_dataset)

        # Variables.
        weights = tf.Variable(
            tf.truncated_normal([image_size * image_size, num_labels]))
        biases = tf.Variable(tf.zeros([num_labels]))

        # Training computation.
        logits = tf.matmul(tf_train_dataset, weights) + biases
        loss = tf.reduce_mean(
            tf.nn.softmax_cross_entropy_with_logits(logits, tf_train_labels))

        # Optimizer.
        optimizer = tf.train.GradientDescentOptimizer(0.5).minimize(loss)

        # Predictions for the training, validation, and test data.
        train_prediction = tf.nn.softmax(logits)
        valid_prediction = tf.nn.softmax(
            tf.matmul(tf_valid_dataset, weights) + biases
        )
        test_prediction = tf.nn.softmax(tf.matmul(tf_test_dataset, weights) + biases)

    # Note: This seems to run a different sample of 128 for 3001 times
    num_steps = 3001

    print(" ## Starting training sample using StochasticGradientDescent")
    timer = Timer()

    with tf.Session(graph=graph) as session:
        tf.initialize_all_variables().run()
        print("Initialized")
        for step in range(num_steps):
            # Pick an offset within the training data, which has been randomized.
            # Note: we could use better randomization across epochs.
            offset = (step * batch_size) % (train_labels.shape[0] - batch_size)
            # Generate a minibatch.
            batch_data = train_dataset[offset:(offset + batch_size), :]
            batch_labels = train_labels[offset:(offset + batch_size), :]
            # Prepare a dictionary telling the session where to feed the minibatch.
            # The key of the dictionary is the placeholder node of the graph to be fed,
            # and the value is the numpy array to feed to it.
            feed_dict = {tf_train_dataset: batch_data, tf_train_labels: batch_labels}
            _, l, predictions = session.run(
                [optimizer, loss, train_prediction], feed_dict=feed_dict)
            if (step % 500 == 0):
                print("Minibatch loss at step %d: %f" % (step, l))
                print("Minibatch accuracy: %.1f%%" % accuracy(predictions, batch_labels))
                print("Validation accuracy: %.1f%%" % accuracy(
                    valid_prediction.eval(), valid_labels))
        print("Test accuracy: %.1f%%" % accuracy(test_prediction.eval(), test_labels))
    print("  - Training completed in %.2f secs" % timer.stop())


def run_StochasticGradientDescentRELU(reformatted):
    print("### Running StochasticGradientDescentRELU")
    train_dataset = reformatted["train"]["dataset"]
    train_labels = reformatted["train"]["labels"]
    valid_dataset = reformatted["valid"]["dataset"]
    valid_labels = reformatted["valid"]["labels"]
    test_dataset = reformatted["test"]["dataset"]
    test_labels = reformatted["test"]["labels"]

    batch_size = 128

    graph = tf.Graph()
    with graph.as_default():

        # Input data. For the training data, we use a placeholder that will be fed
        # at run time with a training minibatch.
        tf_train_dataset = tf.placeholder(tf.float32, shape=(batch_size, image_size * image_size))
        tf_train_labels = tf.placeholder(tf.float32, shape=(batch_size, num_labels))
        tf_valid_dataset = tf.constant(valid_dataset)
        tf_test_dataset = tf.constant(test_dataset)

        hidden_layer_size = 1024
        # Hidden layer 1
        weights_h = tf.Variable(
            tf.truncated_normal([image_size * image_size, hidden_layer_size]))
        biases_h = tf.Variable(tf.zeros([hidden_layer_size]))
        hidden = tf.nn.relu(tf.matmul(tf_train_dataset, weights_h) + biases_h)
        print("hidden: %s" % hidden.get_shape())
        # shape(batch_size, hidden_layer_size)
        # TensorShape([Dimension(128), Dimension(1024)])

        # Output layer
        weights = tf.Variable(
            tf.truncated_normal([hidden_layer_size, num_labels]))
        biases = tf.Variable(tf.zeros([num_labels]))
        logits = tf.matmul(hidden, weights) + biases
        loss = tf.reduce_mean(
            tf.nn.softmax_cross_entropy_with_logits(logits, tf_train_labels)
        )
        print("logits: %s" % logits.get_shape())
        # shape(batch_size, 10)
        # TensorShape([Dimension(128), Dimension(10)])

        # Optimizer.
        optimizer = tf.train.GradientDescentOptimizer(0.5).minimize(loss)

        # Predictions for the training, validation, and test data.
        train_prediction = tf.nn.softmax(logits)

        valid_hidden = tf.nn.relu(tf.matmul(tf_valid_dataset, weights_h) + biases_h)
        valid_prediction = tf.nn.softmax(
            tf.matmul(valid_hidden, weights) + biases
        )

        test_hidden = tf.nn.relu(tf.matmul(tf_test_dataset, weights_h) + biases_h)
        test_logits = tf.matmul(test_hidden, weights) + biases
        test_prediction = tf.nn.softmax(test_logits)

    num_steps = 3001

    print(" ## Starting training sample using StochasticGradientDescent")
    timer = Timer()

    with tf.Session(graph=graph) as session:
        tf.initialize_all_variables().run()
        print("Initialized")
        for step in range(num_steps):
            # Pick an offset within the training data, which has been randomized.
            # Note: we could use better randomization across epochs.
            offset = (step * batch_size) % (train_labels.shape[0] - batch_size)
            # Generate a minibatch.
            batch_data = train_dataset[offset:(offset + batch_size), :]
            batch_labels = train_labels[offset:(offset + batch_size), :]
            # Prepare a dictionary telling the session where to feed the minibatch.
            # The key of the dictionary is the placeholder node of the graph to be fed,
            # and the value is the numpy array to feed to it.
            feed_dict = {tf_train_dataset: batch_data, tf_train_labels: batch_labels}
            _, l, predictions = session.run(
                [optimizer, loss, train_prediction], feed_dict=feed_dict)
            if (step % 500 == 0):
                print("Minibatch loss at step %d: %f" % (step, l))
                print("Minibatch accuracy: %.1f%%" % accuracy(predictions, batch_labels))
                print("Validation accuracy: %.1f%%" % accuracy(
                    valid_prediction.eval(), valid_labels))
        print("Test accuracy: %.1f%%" % accuracy(test_prediction.eval(), test_labels))
    print("  - Training completed in %.2f secs" % timer.stop())

#
# RUN THE SCRIPT
#
if __name__ == "__main__":
    if not os.path.isfile(util.FILE_TRAINING):
        print("%s file does not exist.  Make sure to run step02.py" % util.FILE_TRAINING)
        sys.exit()

    if os.path.isfile(util.FILE_REFORMATTED):
        print("### Loading reformatted data from %s" % util.FILE_REFORMATTED)
        reformatted = util.load_var(util.FILE_REFORMATTED)
    else:
        print("### Loading previously saved data from %s" % util.FILE_TRAINING)
        result = util.load_var(util.FILE_TRAINING)
        reformatted = reformat_result(result)
        print("### Saving reformatted data to %s" % util.FILE_REFORMATTED)
        util.save_var(reformatted, util.FILE_REFORMATTED)

    util.print_stats(reformatted)

    # run_GradientDescentOptimizer(reformatted)
    '''
    Initialized
    Loss at step 0: 18.218006
    Training accuracy: 9.7%
    Validation accuracy: 12.5%
    Loss at step 100: 2.383770
    Training accuracy: 70.7%
    Validation accuracy: 70.7%
    Loss at step 200: 1.895457
    Training accuracy: 74.1%
    Validation accuracy: 72.9%
    Loss at step 300: 1.642863
    Training accuracy: 75.5%
    Validation accuracy: 73.7%
    Loss at step 400: 1.475798
    Training accuracy: 76.3%
    Validation accuracy: 74.1%
    Loss at step 500: 1.353036
    Training accuracy: 77.0%
    Validation accuracy: 74.4%
    Loss at step 600: 1.257370
    Training accuracy: 77.5%
    Validation accuracy: 74.7%
    Loss at step 700: 1.179914
    Training accuracy: 78.0%
    Validation accuracy: 74.9%
    Loss at step 800: 1.115457
    Training accuracy: 78.5%
    Validation accuracy: 75.0%
    Test accuracy: 82.7%
      - Training completed in 29.13 secs

    Learning Rate: 0.9
    -> Test accuracy less than 82.7%
    -> Validation accuracy at step 100 shoudl be greater than 12.5%
    '''

    # run_StochasticGradientDescent(reformatted)
    '''
    Initialized
    Minibatch loss at step 0: 16.270775
    Minibatch accuracy: 14.8%
    Validation accuracy: 17.9%
    Minibatch loss at step 500: 1.573520
    Minibatch accuracy: 74.2%
    Validation accuracy: 75.8%
    Minibatch loss at step 1000: 1.444858
    Minibatch accuracy: 75.8%
    Validation accuracy: 77.5%
    Minibatch loss at step 1500: 1.164824
    Minibatch accuracy: 75.8%
    Validation accuracy: 77.4%
    Minibatch loss at step 2000: 1.035025
    Minibatch accuracy: 80.5%
    Validation accuracy: 77.4%
    Minibatch loss at step 2500: 1.145013
    Minibatch accuracy: 78.9%
    Validation accuracy: 77.3%
    Minibatch loss at step 3000: 1.161497
    Minibatch accuracy: 72.7%
    Validation accuracy: 78.3%
    Test accuracy: 85.6%
      - Training completed in 9.25 secs
    '''

    run_StochasticGradientDescentRELU(reformatted)
    '''
    Minibatch loss at step 0: 444.284058
    Minibatch accuracy: 3.9%
    Validation accuracy: 30.3%
    Minibatch loss at step 500: 33.100132
    Minibatch accuracy: 76.6%
    Validation accuracy: 79.8%
    Minibatch loss at step 1000: 9.802980
    Minibatch accuracy: 82.8%
    Validation accuracy: 80.3%
    Minibatch loss at step 1500: 12.085089
    Minibatch accuracy: 75.8%
    Validation accuracy: 78.4%
    Minibatch loss at step 2000: 4.192687
    Minibatch accuracy: 83.6%
    Validation accuracy: 80.8%
    Minibatch loss at step 2500: 4.897593
    Minibatch accuracy: 85.2%
    Validation accuracy: 82.4%
    Minibatch loss at step 3000: 1.899771
    Minibatch accuracy: 82.0%
    Validation accuracy: 82.3%
    Test accuracy: 89.2%
      - Training completed in 52.11 secs
    '''

'''

--------

y = w * x + b

--------

y1 = w1 * x + b1

r = y1 * abs(y1 > 0)

y2 = w2 * r + b2

--------

y1 = w1 * x + b1

r1 = y1 * abs(y1 > 0)

y2 = w2 * r1 + b2

r2 = y2 * abs(y2 > 0)

y3 = w3 * r2 + b3

'''