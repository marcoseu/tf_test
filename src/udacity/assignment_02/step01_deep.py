#!/usr/bin/env python

'''
SOURCE: https://github.com/tensorflow/tensorflow/blob/master/tensorflow/examples/udacity/1_notmnist.ipynb

Created for:
* Assignment 2
'''

from __future__ import print_function

import os
import sys
import numpy as np
import tensorflow as tf
from six.moves import range

from common import Util as util
from common import Timer

image_size = 28
num_labels = 10

LEARNING_RATE = 0.01
NUM_STEPS = 6001
LAYERS = {
    "output": [
        {"size": 64},
        {"size": 64}
    ]
}


def accuracy(predictions, labels):
    return (
        100.0 * np.sum(
            np.argmax(predictions, 1) == np.argmax(labels, 1)
        ) / predictions.shape[0]
    )


def build_layers(start_dataset, start_size, output_size, dataset_labels):
    dataset = start_dataset
    size = start_size

    # Create hidden layers
    for layer in LAYERS["output"]:
        layer_size = layer["size"]
        layer["weights"] = tf.Variable(
            tf.truncated_normal([size, layer_size])
        )
        layer["biases"] = tf.Variable(tf.zeros([layer_size]))
        layer["logit"] = tf.nn.relu(tf.add(tf.matmul(dataset, layer["weights"]), layer["biases"]))

        size = layer_size
        dataset = layer["logit"]

    # Create output
    weights = tf.Variable(
        tf.truncated_normal([size, output_size]))
    biases = tf.Variable(tf.zeros([output_size]))
    logits = tf.matmul(dataset, weights) + biases
    loss = tf.reduce_mean(
        tf.nn.softmax_cross_entropy_with_logits(logits, dataset_labels)
    )
    return weights, biases, logits, loss


def build_prediction(key, start_dataset, weights, biases):
    dataset = start_dataset
    LAYERS[key] = []
    # Build hidden
    for layer in LAYERS["output"]:
        obj = {}
        obj["hidden"] = tf.nn.relu(tf.add(tf.matmul(dataset, layer["weights"]), layer["biases"]))
        LAYERS[key].append(obj)
        dataset = obj["hidden"]

    # Build Prediction
    prediction = tf.nn.softmax(
        tf.matmul(dataset, weights) + biases
    )

    return prediction


def run_StochasticGradientDescentRELU_deep(reformatted):
    print("### Running StochasticGradientDescentRELU")
    train_dataset = reformatted["train"]["dataset"]
    train_labels = reformatted["train"]["labels"]
    valid_dataset = reformatted["valid"]["dataset"]
    valid_labels = reformatted["valid"]["labels"]
    test_dataset = reformatted["test"]["dataset"]
    test_labels = reformatted["test"]["labels"]

    batch_size = 128

    graph = tf.Graph()
    with graph.as_default():

        # Input data. For the training data, we use a placeholder that will be fed
        # at run time with a training minibatch.
        tf_train_dataset = tf.placeholder(tf.float32, shape=(batch_size, image_size * image_size))
        tf_train_labels = tf.placeholder(tf.float32, shape=(batch_size, num_labels))
        tf_valid_dataset = tf.constant(valid_dataset)
        tf_test_dataset = tf.constant(test_dataset)

        # Build layers
        weights, biases, logits, loss = build_layers(
            tf_train_dataset, image_size * image_size, num_labels, tf_train_labels
        )
        print("logits: %s" % logits.get_shape())

        # Optimizer
        optimizer = tf.train.AdamOptimizer(LEARNING_RATE).minimize(loss)

        # Predictions for the training, validation, and test data.
        train_prediction = tf.nn.softmax(logits)

        # Validation
        valid_prediction = build_prediction("valid", tf_valid_dataset, weights, biases)

        # Test
        test_prediction = build_prediction("test", tf_test_dataset, weights, biases)

    print(" ## Starting training sample using run_StochasticGradientDescentRELU_deep")
    timer = Timer()

    with tf.Session(graph=graph) as session:
        tf.initialize_all_variables().run()
        print("Initialized")
        for step in range(NUM_STEPS):
            # Pick an offset within the training data, which has been randomized.
            # Note: we could use better randomization across epochs.
            offset = (step * batch_size) % (train_labels.shape[0] - batch_size)
            # Generate a minibatch.
            batch_data = train_dataset[offset:(offset + batch_size), :]
            batch_labels = train_labels[offset:(offset + batch_size), :]
            # Prepare a dictionary telling the session where to feed the minibatch.
            # The key of the dictionary is the placeholder node of the graph to be fed,
            # and the value is the numpy array to feed to it.
            feed_dict = {tf_train_dataset: batch_data, tf_train_labels: batch_labels}
            _, l, predictions = session.run(
                [optimizer, loss, train_prediction], feed_dict=feed_dict)
            if (step % 500 == 0):
                print("Minibatch loss at step %d: %f" % (step, l))
                print("Minibatch accuracy: %.1f%%" % accuracy(predictions, batch_labels))
                print("Validation accuracy: %.1f%%" % accuracy(
                    valid_prediction.eval(), valid_labels))
        print("Test accuracy: %.1f%%" % accuracy(test_prediction.eval(), test_labels))
    print("  - Training completed in %.2f secs" % timer.stop())

#
# RUN THE SCRIPT
#
if __name__ == "__main__":
    if not os.path.isfile(util.FILE_REFORMATTED):
        print("%s file does not exist.  Make sure to run step01.py" % util.FILE_TRAINING)
        sys.exit()

    reformatted = util.load_var(util.FILE_REFORMATTED)
    util.print_stats(reformatted)

    run_StochasticGradientDescentRELU_deep(reformatted)
    '''
    NOTE: When using with learning rate of 0.5, we get the error:
    * ReluGrad input is not finite. : Tensor had Inf values

    This was solved by reducing the learning rate to 0.1, but the
    accuracy was greatly reduced.  Following the link below:
    * https://github.com/aymericdamien/TensorFlow-Examples/blob/master/examples/3%20-%20Neural%20Networks/multilayer_perceptron.py

    We managed to improve the accuracy significantly and avoid the error
    `ReluGrad input is not finite. : Tensor had Inf values`


    RESULT
    With AdamOptimizer learning rate of 0.01:
    * steps = 6001, hidden_layer_size = 256

        Minibatch accuracy: 76.6%
        Validation accuracy: 79.8%
        Test accuracy: 87.4%
          - Training completed in 64.00 secs

    * steps = 6001, hidden_layer_size = 128

        Minibatch accuracy: 78.9%
        Validation accuracy: 83.4%
        Test accuracy: 90.2%
          - Training completed in 51.82 secs

    * steps = 6001, hidden_layer_size = 64

        Minibatch accuracy: 80.5%
        Validation accuracy: 83.3%
        Test accuracy: 90.0%
          - Training completed in 42.16 secs

    * steps = 3001, hidden_layer_size = 64

        Minibatch accuracy: 76.6%
        Validation accuracy: 80.0%
        Test accuracy: 87.3%
          - Training completed in 17.66 secs

    * steps = 10001, hidden_layer_size = 128

        Minibatch accuracy: 88.3%
        Validation accuracy: 84.2%
        Test accuracy: 90.8%
          - Training completed in 82.28 secs

    * steps = 3001, hidden_layer_size = 256

        Minibatch accuracy: 76.6%
        Validation accuracy: 75.2%
        Test accuracy: 83.3%
          - Training completed in 34.53 secs

    * steps = 3001, hidden_layer_size = 512

        Minibatch accuracy: 68.0%
        Validation accuracy: 67.1%
        Test accuracy: 75.3%
          - Training completed in 60.65 secs


    With AdamOptimizer learning rate of 0.05:
    * steps = 6001, hidden_layer_size = 512

        Minibatch accuracy: 61.7%
        Validation accuracy: 65.4%
        Test accuracy: 74.0%
          - Training completed in 57.17 secs

    * steps = 6001, hidden_layer_size = 128

        Minibatch accuracy: 6.2%
        Validation accuracy: 10.4%
        Test accuracy: 10.1%
          - Training completed in 55.29 secs
    '''

'''

y1 = w1 * x + b1

r1 = y1 * abs(y1 > 0)

y2 = w2 * r1 + b2

r2 = y2 * abs(y2 > 0)

y3 = w3 * r2 + b3

'''