# Sourcing data

Run the following command to source the data

```
# Make sure you start at the .../src/udacity/ directory

cd data
python setup.py
```
