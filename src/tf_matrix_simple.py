#!/usr/bin/env python

import tensorflow as tf

# Using sample from https://www.mathsisfun.com/algebra/matrix-multiplying.html

# Create a Constant op that produces a 1x2 matrix.  The op is
# added as a node to the default graph.
#
# The value returned by the constructor represents the output
# of the Constant op.
matrix1 = tf.constant([[3., 4., 2.]])

# Create another Constant that produces a 2x1 matrix.
matrix2 = tf.constant([[13., 9., 7., 15.], [8., 7., 4., 6.], [6., 4., 0., 3.]])

# Create a Matmul op that takes 'matrix1' and 'matrix2' as inputs.
# The returned value, 'product', represents the result of the matrix
# multiplication.
product = tf.matmul(matrix1, matrix2)

with tf.Session() as sess:
    result = sess.run(product)
    print(result)

# ==> [[ 83.  63.  37.  75.]]
