#!/usr/bin/env python

from __future__ import print_function

import tensorflow as tf

dataset = tf.constant([[0., 1.], [1., 0.], [0., 2.], [2., 0.]], dtype='float32')
print("dataset: ", dataset)

image_size = 2
sample_size = 2

r_data = tf.truncated_normal([image_size * image_size, sample_size])
print("r_data: ", r_data.get_shape())
weights = tf.Variable(r_data)
print("w: ", weights.get_shape())

biases = tf.Variable(tf.zeros([sample_size]))
print("b: ", biases.get_shape())

logits = tf.matmul(dataset, weights)
print("logits: ", logits.get_shape())

with tf.Session() as sess:
    result = sess.run(logits)
    print(result)
