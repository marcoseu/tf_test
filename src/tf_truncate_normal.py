#!/usr/bin/env python

'''

This script shows what a truncated normal do, which is a 
normally distributed random value with stdev=1 and mean=0.

The value above 2x stdev is dropped, hence "truncated"


shape(train_size, 25)
x
shape(25, 10)
=
shape(train_size, 10)

'''


from __future__ import print_function

import numpy
import tensorflow as tf

image_size = 5
num_labels = 10

weights = tf.Variable(
    tf.truncated_normal([image_size * image_size, num_labels])
)

relu = tf.nn.relu(weights)


def round_num(input):
    return numpy.around(input, 2)


with tf.Session() as sess:
    tf.initialize_all_variables().run()
    result, r = sess.run([weights, relu])
    print("### RESULT:")
    print(round_num(result))

    print("### RELU:")
    print(round_num(r))


