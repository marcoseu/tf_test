#!/usr/bin/env python

# SOURCE: http://machinelearningmastery.com/get-your-hands-dirty-with-scikit-learn-now/

# Logistic Regression
from __future__ import print_function

from sklearn import datasets
from sklearn import metrics
from sklearn.linear_model import LogisticRegression
# load the iris datasets
dataset = datasets.load_iris()
# fit a logistic regression model to the data
model = LogisticRegression()

print("data:\n", dataset.data)
print("target:\n", dataset.target)

model.fit(dataset.data, dataset.target)
print(model)
# make predictions
expected = dataset.target
predicted = model.predict(dataset.data)
# summarize the fit of the model
print(metrics.classification_report(expected, predicted))
print(metrics.confusion_matrix(expected, predicted))
