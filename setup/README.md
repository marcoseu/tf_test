# Installing tensorflow

1. virtualenv venv
1. . venv/bin/activate
1. easy_install --upgrade six
1. pip install --upgrade https://storage.googleapis.com/tensorflow/mac/tensorflow-0.6.0-py2-none-any.whl
1. pip install matplotlib

REF: https://www.tensorflow.org/versions/master/get_started/os_setup.html

## Replace active script

cd venv/bin
ln -s ../../setup/activate ./

REF: http://matplotlib.org/faq/virtualenv_faq.html
